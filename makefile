DIRECTORY = $(addprefix Debug/,$(sort $(dir $(wildcard src/*/))))
CC := gcc
CCFLAGS :=
CC_SRCS := $(shell find -name '*.c')
CC_OBJS := $(addprefix Debug/,$(patsubst ./%,%,$(patsubst %.c,%.o,$(CC_SRCS))))

CXX := g++
CXXFLAGS := -std=c++0x -I/usr/local/include -I/usr/local/include/opencv4 -O0 -g0 -Wall -fmessage-length=0 -march=native

CXX_SRCS := $(shell find -name '*.cpp')
CXX_OBJS := $(addprefix Debug/,$(patsubst ./%,%,$(patsubst %.cpp,%.o,$(CXX_SRCS))))
USER_OBJS := 
LIBS := -L/usr/local/lib -L/opt/intel/opencl -lOpenCL -lopencv_core -lopencv_imgcodecs -lopencv_imgproc -lopencv_highgui -lopencv_ml -lopencv_videoio -lopencv_features2d -lopencv_calib3d -lopencv_objdetect -lopencv_flann -lpthread

LIBS_CUDA:=-lopencv_cudaimgproc -lopencv_cudaarithm -lopencv_cudabgsegm -lopencv_cudacodec -lopencv_cudafeatures2d -lopencv_cudafilters -lopencv_cudalegacy -lopencv_cudaobjdetect -lopencv_cudaoptflow -lopencv_cudastereo -lopencv_cudawarping -lopencv_cudev

EXECUTABLES := Barebone-Linux-OpenCV
# Add inputs and outputs from these tool invocations to the build variables 

# All Target
all: err
cpu: clean makeDirectory exe
cuda: clean makeDirectory exe_cuda
	
$(CC_OBJS) : %.o :
	$(CC) $(CCFLAGS) -c $(patsubst Debug/%,%,$(patsubst %.o,%.c,$@)) -o $@

$(CXX_OBJS) : %.o :
	$(CXX) $(CXXFLAGS) -c $(patsubst Debug/%,%,$(patsubst %.o,%.cpp,$@)) -o $@


exe: $(CC_OBJS) $(CXX_OBJS)
	@echo 'Building target: CPU'# $@
	@echo 'Invoking: GCC C++ Linker'
	$(CXX) $(CXXFLAGS) -o Debug/$(EXECUTABLES) $(CC_OBJS) $(CXX_OBJS) $(USER_OBJS) $(LIBS)
	@echo 'Finished building target: $(EXECUTABLES)'
	@echo ' '

exe_cuda: $(CC_OBJS) $(CXX_OBJS)
	@echo 'Building target with Cuda: Cuda'# $@
	@echo 'Invoking: GCC C++ Linker'
	$(CXX) $(CXXFLAGS) -o Debug/$(EXECUTABLES) $(CC_OBJS) $(CXX_OBJS) $(USER_OBJS) $(LIBS) $(LIBS_CUDA)
	@echo 'Finished building target: $(EXECUTABLES)'
	@echo ' '

makeDirectory:
	mkdir -p Debug/ $(DIRECTORY)

clean :
	rm -R Debug | echo ''

err :
	@echo 'Error make command! Either use "make cpu" or "make cuda"!!'

.PHONY: all clean
