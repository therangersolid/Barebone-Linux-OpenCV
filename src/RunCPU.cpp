///*------------------------------------------------------*
// * Run.cpp                                        *
// * OpenCV's Hello World!!!                   *
// *------------------------------------------------------*/
//
///**
// * This is for the definition of cv::mat
// */
//#include "opencv2/core/core.hpp"
//
///**
// * This is for the imread function
// */
//#include "opencv2/imgcodecs.hpp"
//
///**
// * Image processing functions with the cvtColor
// */
//#include "opencv2/imgproc/imgproc.hpp"
//
///**
// * Display the window with results
// */
//#include "opencv2/highgui/highgui.hpp"
//#include <stdio.h>
//
//cv::Mat src;
//cv::Mat src_gray;
//
//int main(int, char** argv) {
//	src = cv::imread("stop-2018-07-03.png");
//
//	// CPU part:
//	cvtColor(src, src_gray, cv::COLOR_BGR2GRAY);
//
//	const char* source_window = "SourceCPU";
//	namedWindow(source_window, cv::WINDOW_AUTOSIZE);
//	imshow(source_window, src_gray);
//
//	cv::waitKey(0);
//	return (0);
//
//}
