////============================================================================
//// Name        : Barebone-OpenCV.cpp
//// Author      :
//// Version     :
//// Copyright   : Your copyright notice
//// Description : Hello World in C++, Ansi-style
////============================================================================
//
////NO contours
//#include <iostream>
//#include <fstream>
//#include <string>
//#include "opencv2/opencv_modules.hpp"
//#include "opencv2/highgui/highgui.hpp"
//#include "opencv2/imgproc.hpp"
//
//using namespace std;
//using namespace cv;
//
//int low_h = 0, low_s = 18, low_v = 0;  //global var, emperical
//int high_h = 73, high_s = 255, high_v = 250; //global var
//
//int main(int argc, char **argv) {
//	bool playVideo = true;
//	VideoCapture cap(argv[1]);  //command line input video file
//	VideoCapture cap2(argv[2]);  //command line input video file
//	if (!cap.isOpened()||!cap2.isOpened()) {
//		cout << "Unable to open video " << argv[1] << " or " <<  argv[2] << "\n";
//		return 0;
//	}
//
//	Mat frame, frame_threshold, frame_hsv, frame_result, frame_half,
//			frame_double, frame_gaussian;
//
//	/////////////////////////////////////////////
//	Mat frame2, frame_down, frame_tmp, frame_blur;
//	frame_tmp = frame2;
//
//	namedWindow("Floor1", WINDOW_NORMAL);
//	///////////////////////////////////////////
//	namedWindow("Video", WINDOW_NORMAL);
//	namedWindow("hsv", WINDOW_NORMAL);
//	namedWindow("HalfSize", WINDOW_NORMAL);
//	namedWindow("DoubleSize", WINDOW_NORMAL);
//	namedWindow("GaussianBlur", WINDOW_NORMAL);
//	////////////////////////////////////////////
//	while (1) {
//		if (playVideo)
//			cap >> frame;
////		    cap2 >> frame2;
//		if (frame.empty()) {
//			cout << "Empty Frame\n";
//			return 0;
//		}
//
//		cvtColor(frame, frame_hsv, cv::COLOR_BGR2HSV);
//		pyrDown(frame, frame_half, Size(frame.cols / 2, frame.rows / 2));
//		for (int i = 0; i < 4; i++) {
//			pyrDown(frame_half, frame_half,
//					Size(frame_half.cols / 2, frame_half.rows / 2));
//		}
//		pyrUp(frame_half, frame_double,
//				Size(frame_half.cols * 2, frame_half.rows * 2));
//		for (int i = 0; i < 4; i++) {
//			pyrUp(frame_double, frame_double,
//					Size(frame_double.cols * 2, frame_double.rows * 2));
//		}
//		///
//		GaussianBlur(frame_double, frame_gaussian,
//				Size(31, 31), 0, 0);
//
//		imshow("Video", frame);
//		imshow("hsv", frame_hsv);
//		imshow("HalfSize", frame_half);
//		imshow("DoubleSize", frame_double);
//		imshow("GaussianBlur", frame_gaussian);
//		/////////////////////////////////////////////////
////		imshow("Floor1", frame2);
//
//		char key = waitKey(5);
//		if (key == 'p')
//			playVideo = !playVideo;
//	}
//	return 0;
//}
//
