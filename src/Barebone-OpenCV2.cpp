////============================================================================
//// Name        : Barebone-OpenCV.cpp
//// Author      :
//// Version     :
//// Copyright   : Your copyright notice
//// Description : Hello World in C++, Ansi-style
////============================================================================
//
////NO contours
//#include <iostream>
//#include <fstream>
//#include <string>
//#include "opencv2/opencv_modules.hpp"
//#include "opencv2/highgui/highgui.hpp"
//#include "opencv2/imgproc.hpp"
//#include <unistd.h>
//
//using namespace std;
//using namespace cv;
//
//int main(int argc, char **argv) {
//	bool playVideo = true;
//	VideoCapture cap(argv[1]);  //command line input video file
//	if (!cap.isOpened()) {
//		cout << "Unable to open video " << argv[1] << "\n";
//		return 0;
//	}
//
//	/////////////////////////////////////////////
//	Mat frame, frame_down, frame_tmp, frame_blur;
//	Mat frame_binary, frame_blur_gray;
//	Mat frame_tmp2;
//	Mat frame_canny;
//	Mat frame_leftBox,frame_rightBox;
//	namedWindow("Floor1", WINDOW_NORMAL);
//	namedWindow("FloorDown", WINDOW_NORMAL);
//	namedWindow("FloorMean", WINDOW_NORMAL);
//	namedWindow("FloorBinary", WINDOW_NORMAL);
//	namedWindow("FloorCanny", WINDOW_NORMAL);
//	namedWindow("FloorLeft", WINDOW_NORMAL);
//	namedWindow("FloorRight", WINDOW_NORMAL);
//	////////////////////////////////////////////
//	while (1) {
//		if (playVideo)
//			cap >> frame;
//		if (frame.empty()) {
//			cout << "Empty Frame\n";
//			return 0;
//		}
//////////// Video Processing here
//		frame_tmp = frame;
//		for (int i = 0; i <= 1; i++) {
//			pyrDown(frame_tmp, frame_down,
//					Size(frame_tmp.cols / 2, frame_tmp.rows / 2));
//
//			pyrDown(frame_down, frame_tmp,
//					Size(frame_down.cols / 2, frame_down.rows / 2));
//		}
//		GaussianBlur(frame_tmp, frame_blur, Size(5, 5), 20.0, 20.0);
//
//		cvtColor(frame_blur, frame_blur_gray, COLOR_BGR2GRAY);
//
//		cv::Scalar mean, stddev; //0:1st channel, 1:2nd channel and 2:3rd channel.
//		meanStdDev(frame_blur_gray, mean, stddev, cv::Mat());
//		threshold(frame_blur_gray, frame_binary,
//				mean.val[0] - 0.5 * stddev.val[0], 255, 0);
//		frame_tmp2 = frame_binary;
//		dilate(frame_tmp2, frame_binary, Mat(), Point(-1, -1), 3, 1, 1);
//
//		// 10 is the weak/low. Changing these reduce a noise.
//		// 100 is the strong/upper
//		// to use different strength of these
//		// 3 is aperture, size of the kernel, maximum is seven. if we want the feature use 3.
//
//		Canny(frame_binary,frame_canny,10,100,3); //Apply canny edge detector.
//
//		// LoG edge detector LoG(x,y;Z1,Z2)*I(x,y)// You can use 2D filter, you can use compute kernel value.
//#define col_left_path_ROI 0
//#define row_left_path_ROI 0
//#define height_left_path_ROI 20
//#define width_left_path_ROI 10
//
//#define col_right_path_ROI 40
//#define row_right_path_ROI 40
//#define height_right_path_ROI 20
//#define width_right_path_ROI 10
//
//		Rect roi_left = Rect(col_left_path_ROI, row_left_path_ROI,
//		width_left_path_ROI, height_left_path_ROI);
//		Rect roi_right = Rect(col_right_path_ROI, row_right_path_ROI,
//				width_right_path_ROI, height_right_path_ROI);
//		frame_leftBox = frame_blur_gray(roi_left);
////		frame_rightBox = frame_blur_gray(roi_right);
//		imshow("Floor1", frame);
//		imshow("FloorDown", frame_down);
//		imshow("FloorMean", frame_blur_gray);
//		imshow("FloorBinary", frame_binary);
//		imshow("FloorCanny", frame_canny);
//		imshow("FloorLeft", frame_leftBox);
////		imshow("FloorRight", frame_rightBox);
//		char key = waitKey(5);
//		if (key == 'p')
//			playVideo = !playVideo;
//
//		// Slow down.
//		usleep(60000);
//	}
//	return 0;
//}
//
