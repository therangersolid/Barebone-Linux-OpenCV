//============================================================================
// Name        : Run.cpp
// Author      :
// Version     :
// Copyright   :
// Description : Validity of the opencl's thread and inner working
//============================================================================

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h> // For sleep

// Features exist in the opencv like HAVE_CUDA && HAVE_OPENCL
#include <opencv2/cvconfig.h>

#ifdef HAVE_OPENCL
// Include opencl
#define CL_TARGET_OPENCL_VERSION 120
#include <CL/opencl.h>

#define MAX_SOURCE_SIZE (0x100000)

#define MY_MAX_PLATFORM 10

#define MY_MAX_DEVICE 10 // Per platform
#endif


// Add for camera input
#include <opencv2/opencv.hpp>

// This is for the definition of cv::mat
#include <opencv2/core/core.hpp>

// This is for the imread function
#include <opencv2/imgcodecs.hpp>

// Image processing functions with the cvtColor
#include <opencv2/imgproc/imgproc.hpp>

// Display the window with results
#include <opencv2/highgui/highgui.hpp>

#ifdef HAVE_OPENCL
#include <opencv2/core/ocl.hpp>
#endif

#ifdef HAVE_CUDA // We assume if we have this, then the OpenCV is compiled with CUDA
// CUDA Counterparts:
#include <opencv2/core/cuda.hpp>

// Image processing functions with the cuda::cvtColor
#include <opencv2/cudaimgproc.hpp>

// This is for implementing the gaussian blur for cuda
#include <opencv2/cudafilters.hpp>
#endif
// Pthread for the multithreading
#include <pthread.h>

#include <stdio.h>
// Chrono is for timer
#include <chrono>

// For getting tid
#include <sys/syscall.h>   /* For SYS_xxx definitions */
// For getting pid
#include <unistd.h>
// For setting priority
#include <sys/resource.h>

// get the ubuntu data, etc
#include <sys/utsname.h>

#include <string>

// This is for passing to pthread as arguments
struct CannyCPUStruct {
	int threadId;
	int numOfIteration;
	cv::Mat* src;
	cv::Mat* srcGray;
	cv::Mat* srcBlur;
	cv::Mat* canny;
};
#ifdef HAVE_OPENCL
struct CannyCLStruct {
	int threadId;
	int numOfIteration;
	cv::UMat* clSrc;
	cv::UMat* clSrcGray;
	cv::UMat* clSrcBlur;
	cv::UMat* clCanny;
};
#endif

#ifdef HAVE_CUDA
// This is for passing to pthread as arguments
struct CannyGPUStruct {
	int threadId;
	int numOfIteration;
	cv::cuda::GpuMat* gpuSrc;
	cv::cuda::GpuMat* gpuSrcGray;
	cv::cuda::GpuMat* gpuSrcBlur;
	cv::cuda::GpuMat* gpuCanny;
	cv::Ptr<cv::cuda::CannyEdgeDetector>* ced;
	cv::Ptr<cv::cuda::Filter>* filter;

};
#endif

#define totNumIteration 2000

int thresh = 100;
#define max_thresh 255

/**
 * Note on opencv Mat operation:
 * Mat A = imread(argv[1], CV_LOAD_IMAGE_COLOR);

 * Mat B = A.clone();  // B is a deep copy of A. (has its own copy of the pixels)
 * Mat C = A;          // C is a shallow copy of A ( rows, cols copied, but shared pixel-pointer )
 * Mat D; A.copyTo(D); // D is a deep copy of A, like B
 */

/* The threads */
void *cannyCPU(void *arguments) {
	struct CannyCPUStruct* cannyCPUStruct = (struct CannyCPUStruct*) arguments;
	pid_t tid = syscall(SYS_gettid);
	printf("tid of CPU[%i] %i\n", cannyCPUStruct->threadId, tid);
	int ret = setpriority(PRIO_PROCESS, tid, 19); // set as highest niceness (low priority) in ubuntu
	cv::Mat src = *(cannyCPUStruct->src); // shallow copy
	cv::Mat srcGray = *(cannyCPUStruct->srcGray); // shallow copy
	cv::Mat srcBlur = *(cannyCPUStruct->srcBlur); // shallow copy
	cv::Mat canny = *(cannyCPUStruct->canny); // shallow copy
	int numOfIteration = cannyCPUStruct->numOfIteration;
	for (int i = 0; i < numOfIteration; ++i) {
		cv::cvtColor(src, srcGray, cv::COLOR_BGR2GRAY);
		cv::GaussianBlur(srcGray, srcBlur, cv::Size(3, 3), 0, 0);
		cv::Canny(srcBlur, canny, thresh, thresh * 2, 3);
	}
	printf("CPU[%i] Done!\n", cannyCPUStruct->threadId);
	/* the function must return something - NULL will do */
	return NULL;
}

#ifdef HAVE_OPENCL
void *cannyCL(void *arguments) {
	struct CannyCLStruct* cannyCLStruct = (struct CannyCLStruct*) arguments;
	pid_t tid = syscall(SYS_gettid);
	printf("tid of CL[%i] %i\n", cannyCLStruct->threadId, tid);
	int ret = setpriority(PRIO_PROCESS, tid, 19); // set as highest niceness (low priority) in ubuntu
	cv::UMat clSrc = *(cannyCLStruct->clSrc); // shallow copy
	cv::UMat clSrcGray = *(cannyCLStruct->clSrcGray); // shallow copy
	cv::UMat clSrcBlur = *(cannyCLStruct->clSrcBlur); // shallow copy
	cv::UMat clCanny = *(cannyCLStruct->clCanny); // shallow copy
	int numOfIteration = cannyCLStruct->numOfIteration;
	for (int i = 0; i < numOfIteration; ++i) {
		cv::cvtColor(clSrc, clSrcGray, cv::COLOR_BGR2GRAY);
		cv::GaussianBlur(clSrcGray, clSrcBlur, cv::Size(3, 3), 0, 0);
		cv::Canny(clSrcBlur, clCanny, thresh, thresh * 2, 3);
	}
	printf("CL[%i] Done!\n", cannyCLStruct->threadId);
	/* the function must return something - NULL will do */
	return NULL;
}
#endif

#ifdef HAVE_CUDA
void *cannyGPU(void *arguments) {
	struct CannyGPUStruct* cannyGPUStruct = (struct CannyGPUStruct*) arguments;
	pid_t tid = syscall(SYS_gettid);
	printf("tid of GPU[%i] %i\n", cannyGPUStruct->threadId, tid);
	int ret = setpriority(PRIO_PROCESS, tid, -19); // set as lowest niceness (high priority) in ubuntu
	cv::cuda::GpuMat gpuSrc = *(cannyGPUStruct->gpuSrc);// shallow copy
	cv::cuda::GpuMat gpuSrcGray = *(cannyGPUStruct->gpuSrcGray);// shallow copy
	cv::cuda::GpuMat gpuSrcBlur = *(cannyGPUStruct->gpuSrcBlur);// shallow copy
	cv::cuda::GpuMat gpuCanny = *(cannyGPUStruct->gpuCanny);// shallow copy
	cv::Ptr<cv::cuda::CannyEdgeDetector> ced = *(cannyGPUStruct->ced);
	cv::Ptr<cv::cuda::Filter> filter = *(cannyGPUStruct->filter);
	int numOfIteration = cannyGPUStruct->numOfIteration;
	for (int i = 0; i < numOfIteration; ++i) {
		cv::cuda::cvtColor(gpuSrc, gpuSrcGray, cv::COLOR_BGR2GRAY);
		filter->apply(gpuSrcGray, gpuSrcBlur);
		ced->detect(gpuSrcBlur, gpuCanny);
	}
	printf("GPU[%i] Done!\n", cannyGPUStruct->threadId);
	/* the function must return something - NULL will do */
	return NULL;
}
#endif
#include <unistd.h> // For sleep
#include <vector>

int main(int, char** argv) {
	// Printing the overall system status:
	printf("================ OpenCV Status ==================\n");
	printf("Compiled with: %s\n", CV_VERSION);
	printf("Running OpenCV:\n");
	printf("%s\n", cv::getBuildInformation().c_str());
	// Check if using optimized code
	if (cv::useOptimized()) {
		printf("Using optimized code\n");
	}
	printf("OpenCV's number of threads: %i\n", cv::getNumThreads());
	printf("Total number of threads generated = %i\n", cv::getNumberOfCPUs());

	// Number of processor online (Through system calls):
	int totNumThread = sysconf(_SC_NPROCESSORS_ONLN); // Including one parent.
	printf("Total number of processor detected = %i\n", totNumThread);

	if ((totNumThread != cv::getNumberOfCPUs())
			&& (totNumThread != cv::getNumThreads())) {
		printf("The numbers above are different! Contact support!");
	}
	// Setting the number of thread:
//	cv::setNumThreads(0);
	// Compile opencv without openmp ()& cv::setNumThreads(1);
	printf("==================================================\n");
	// Checking the scheduler of the linux:
	const int policy = sched_getscheduler(0);
	printf("The policy is [");
	switch (policy) {
	case SCHED_BATCH:
		printf("SCHED_BATCH");
		break;
	case SCHED_FIFO:
		printf("SCHED_FIFO");
		break;
	case SCHED_IDLE:
		printf("SCHED_IDLE");
		break;
	case SCHED_OTHER:
		printf("SCHED_OTHER");
		break;
	case SCHED_RESET_ON_FORK:
		printf("SCHED_RESET_ON_FORK");
		break;
	case SCHED_RR:
		printf("SCHED_RR");
		break;
	default:
		printf("unknown sched!");
		break;
	}
	printf("]\n");
	int maxPrio = sched_get_priority_max(policy);
	int minPrio = sched_get_priority_min(policy);
	if ((maxPrio == -1) || (minPrio == -1)) {
		printf("Receiving priority error!, errno = %d\n", errno);
		return (1);
	}
	printf("Highest priority is = %i\n", maxPrio);
	printf("Lowest priority is = %i\n", minPrio);
	printf("Set GPU priority to max\n");
	printf("Set CPU priority to min\n");
	pthread_attr_t gPU_t_attr;
	pthread_attr_t cPU_t_attr;
	int ret = 0;
	/* initialized with default attributes */
	ret = pthread_attr_init(&gPU_t_attr);
	ret = pthread_attr_init(&cPU_t_attr);
	sched_param gPU_param;
	sched_param cPU_param;
	/* safe to get existing scheduling param */
	ret = pthread_attr_getschedparam(&gPU_t_attr, &gPU_param);
	ret = pthread_attr_getschedparam(&cPU_t_attr, &cPU_param);
	/* set the priority; others are unchanged */
	gPU_param.sched_priority = maxPrio;
	cPU_param.sched_priority = minPrio;
	/* setting the new scheduling param */
	ret = pthread_attr_setschedparam(&gPU_t_attr, &gPU_param);
	ret = pthread_attr_setschedparam(&gPU_t_attr, &cPU_param);
	///////////////////////////////////////////////
	// Niceness value: (when the policy is other)
	// Check and set this thread priority first by these code:
	// Get the thread ID
	pid_t tid = syscall(SYS_gettid);
	if (tid == -1) {
		printf("Failed to get the thread ID, errno = %d\n", errno);
		return (1);
	}
	ret = getpriority(PRIO_PROCESS, tid);
	printf("tid of Parent %i\n", tid);
	printf("Parent's niceness %i\n", ret);

	cv::Mat src; // Read only data can go in single array
	cv::Mat srcGray[totNumThread];
	cv::Mat srcBlur[totNumThread];
	cv::Mat canny[totNumThread];

	// Video capture
	src = cv::imread("nvidia-logo.jpg");
	// Skip 1st call gpu api because 1st call might consume lots of time
	cv::cvtColor(src, srcGray[0], cv::COLOR_BGR2GRAY);
	cv::GaussianBlur(srcGray[0], srcBlur[0], cv::Size(3, 3), 0, 0);
	cv::Canny(srcBlur[0], canny[0], thresh, thresh * 2, 3);

//	cv::VideoCapture cap("nvcamerasrc ! video/x-raw(memory:NVMM), width=(int)1280, height=(int)720, format=(string)I420, framerate=(fraction)120/1 ! \
//                    nvvidconv   ! video/x-raw, format=(string)BGRx ! \
//                    videoconvert ! video/x-raw, format=(string)BGR ! \
//                    appsink");
//
//	if (!cap.isOpened()) {
//		return -1;
//	}
//	cv::namedWindow("Camera", cv::WINDOW_AUTOSIZE);
//	for (i = 0; i < 10; ++i) {
//		cap >> src;
//		cv::imshow("Camera", src);
//	}

#ifdef HAVE_CUDA
	cv::cuda::GpuMat gpuSrc;
	cv::cuda::GpuMat gpuSrcGray;
	cv::cuda::GpuMat gpuSrcBlur;
	cv::cuda::GpuMat gpuCanny;

	cv::Ptr<cv::cuda::CannyEdgeDetector> ced;
	cv::Ptr<cv::cuda::Filter> filter;

	float cpuLoad = 0.80f;
	float gpuLoad = 1 - cpuLoad;

	// Skip 1st call gpu api because 1st call might consume lots of time
	gpuSrc.upload(src);
	// CED & Filter
	ced = cv::cuda::createCannyEdgeDetector(thresh, thresh * 2, 3);
	filter = cv::cuda::createGaussianFilter(gpuSrcGray.type(),
			gpuSrcBlur.type(), cv::Size(3, 3), 0, 0);
	// Warmup the GPU
	cv::cuda::cvtColor(gpuSrc, gpuSrcGray, cv::COLOR_BGR2GRAY);
	filter->apply(gpuSrcGray, gpuSrcBlur);
	ced->detect(gpuSrcBlur, gpuCanny);
#endif
#ifdef HAVE_OPENCL
	cv::UMat clSrc;
	cv::UMat clSrcGray;
	cv::UMat clSrcBlur;
	cv::UMat clCanny;
	// Opencl in OpenCV
	if (cv::ocl::haveOpenCL()) {
		printf("OpenCL is available\n");
		std::cout << "==============" << std::endl;
		cv::ocl::Device device = cv::ocl::Device::getDefault();
		std::cout << "name: " << device.name() << std::endl;
		std::cout << "available: " << device.available() << std::endl;
		std::cout << "img support: " << device.imageSupport() << std::endl;
		std::cout << "cl v: " << device.OpenCL_C_Version() << std::endl;
		std::cout << "==============" << std::endl;
		cv::ocl::setUseOpenCL(true);

		src.copyTo(clSrc);
		// Skip 1st call gpu api because 1st call might consume lots of time
		cv::cvtColor(clSrc, clSrcGray, cv::COLOR_BGR2GRAY);
		cv::GaussianBlur(clSrcGray, clSrcBlur, cv::Size(3, 3), 0, 0);
		cv::Canny(clSrcBlur, clCanny, thresh, thresh * 2, 3);
	}
#endif
	/**
	 * CPU Parts
	 */
	pthread_t cannyCPUSoloThread;
	struct CannyCPUStruct cannyCPUSoloStruct;
	cannyCPUSoloStruct.numOfIteration = totNumIteration; // 100% load
	cannyCPUSoloStruct.src = &src;
	cannyCPUSoloStruct.srcGray = &srcGray[0];
	cannyCPUSoloStruct.srcBlur = &srcBlur[0];
	cannyCPUSoloStruct.canny = &canny[0];
	cannyCPUSoloStruct.threadId = 0;

	auto startCPUSolo = std::chrono::system_clock::now();

	/* create the threads which executes canny edge detection in cpu */
	if (pthread_create(&cannyCPUSoloThread, &cPU_t_attr, cannyCPU,
			(void*) &cannyCPUSoloStruct)) {
		printf("Error creating thread[%i]!\n", cannyCPUSoloStruct.threadId);
		return 1;
	}
	/* wait for the threads to finish */
	if (pthread_join(cannyCPUSoloThread, NULL)) {
		printf("Error joining thread[%i]!\n", cannyCPUSoloStruct.threadId);
		return 2;
	}
	auto finishCPUSolo = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsedTimeCPUSolo = finishCPUSolo
			- startCPUSolo;
	printf("CPU Solo Elapsed Time: %f msecs\n",
			elapsedTimeCPUSolo.count() * 1000);
#ifdef HAVE_CUDA
	/**
	 * GPU Single Parts
	 */
	pthread_t cannyGPUSoloThread;
	struct CannyGPUStruct cannyGPUSoloStruct;
	cannyGPUSoloStruct.numOfIteration = totNumIteration; // 100% load
	cannyGPUSoloStruct.gpuSrc = &gpuSrc;
	cannyGPUSoloStruct.gpuSrcGray = &gpuSrcGray;
	cannyGPUSoloStruct.gpuSrcBlur = &gpuSrcBlur;
	cannyGPUSoloStruct.gpuCanny = &gpuCanny;
	cannyGPUSoloStruct.filter = &filter;
	cannyGPUSoloStruct.ced = &ced;
	cannyGPUSoloStruct.threadId = 0;

	auto startGPUSolo = std::chrono::system_clock::now();

	/* create the threads which executes canny edge detection in Gpu */
	if (pthread_create(&cannyGPUSoloThread, &gPU_t_attr, cannyGPU,
					(void*) &cannyGPUSoloStruct)) {
		printf("Error creating thread[%i]!\n", cannyGPUSoloStruct.threadId);
		return 1;
	}
	/* wait for the threads to finish */
	if (pthread_join(cannyGPUSoloThread, NULL)) {
		printf("Error joining thread[%i]!\n", cannyCPUSoloStruct.threadId);
		return 2;
	}
	auto finishGPUSolo = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsedTimeGPUSolo = finishGPUSolo
	- startGPUSolo;
	printf("GPU Solo Elapsed Time: %f msecs\n",
			elapsedTimeGPUSolo.count() * 1000);
	printf("Time improvement= %f %\n",
			(elapsedTimeCPUSolo.count() - elapsedTimeGPUSolo.count())
			/ elapsedTimeCPUSolo.count() * 100);
#endif
#ifdef HAVE_OPENCL
	/**
	 * OpenCL Single Parts
	 */
	pthread_t cannyCLSoloThread;
	struct CannyCLStruct cannyCLSoloStruct;
	cannyCLSoloStruct.numOfIteration = totNumIteration; // 100% load
	cannyCLSoloStruct.clSrc = &clSrc;
	cannyCLSoloStruct.clSrcGray = &clSrcGray;
	cannyCLSoloStruct.clSrcBlur = &clSrcBlur;
	cannyCLSoloStruct.clCanny = &clCanny;
	cannyCLSoloStruct.threadId = 0;

	auto startCLSolo = std::chrono::system_clock::now();

	/* create the threads which executes canny edge detection in cpu */
	if (pthread_create(&cannyCLSoloThread, &gPU_t_attr, cannyCL,
			(void*) &cannyCLSoloStruct)) {
		printf("Error creating thread[%i]!\n", cannyCLSoloStruct.threadId);
		return 1;
	}
	/* wait for the threads to finish */
	if (pthread_join(cannyCPUSoloThread, NULL)) {
		printf("Error joining thread[%i]!\n", cannyCLSoloStruct.threadId);
		return 2;
	}
	auto finishCLSolo = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsedTimeCLSolo = finishCLSolo
			- startCLSolo;
	printf("CL Solo Elapsed Time: %f msecs\n",
			elapsedTimeCLSolo.count() * 1000);
#endif
	/**
	 * CPU Multi Parts
	 */
	pthread_t cannyCPUMultiThread[totNumThread];
	struct CannyCPUStruct cannyCPUMultiStruct[totNumThread];
	for (int t = 0; t < totNumThread; ++t) {
		cannyCPUMultiStruct[t].numOfIteration = totNumIteration / totNumThread; // Split evenly on all core
		cannyCPUMultiStruct[t].src = &src;
		cannyCPUMultiStruct[t].srcGray = &srcGray[t];
		cannyCPUMultiStruct[t].srcBlur = &srcBlur[t];
		cannyCPUMultiStruct[t].canny = &canny[t];
		cannyCPUMultiStruct[t].threadId = t;
	}
	auto startCPUMulti = std::chrono::system_clock::now();

	/* create the threads which executes canny edge detection in gpu */
	for (int t = 0; t < totNumThread; ++t) {
		if (pthread_create(&cannyCPUMultiThread[t], &cPU_t_attr, cannyCPU,
				(void*) &cannyCPUMultiStruct[t])) {
			printf("Error creating thread[%i]!\n",
					cannyCPUMultiStruct[t].threadId);
			return 1;
		}
	}
	/* wait for the threads to finish */
	for (int t = 0; t < totNumThread; ++t) {
		if (pthread_join(cannyCPUMultiThread[t], NULL)) {
			printf("Error joining thread[%i]!\n",
					cannyCPUMultiStruct[t].threadId);
			return 2;
		}
	}
	auto finishCPUMulti = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsedTimeCPUMulti = finishCPUMulti
			- startCPUMulti;
	printf("CPU Multi Elapsed Time: %f msecs\n",
			elapsedTimeCPUMulti.count() * 1000);
	printf("Time improvement= %f %\n",
			(elapsedTimeCPUSolo.count() - elapsedTimeCPUMulti.count())
					/ elapsedTimeCPUSolo.count() * 100);

#ifdef HAVE_CUDA
	/**
	 * Hybrids 2 cpu thread + 1 gpu (Parent allocate the gpu)
	 */
	pthread_t cannyCPUSoloGPUThread;
	struct CannyCPUStruct cannyCPUSoloGPUStruct;
	cannyCPUSoloGPUStruct.numOfIteration = (int) ((float) totNumIteration
			* 0.70f/*cpuLoad*/);
	cannyCPUSoloGPUStruct.src = &src;
	cannyCPUSoloGPUStruct.srcGray = &srcGray[0];
	cannyCPUSoloGPUStruct.srcBlur = &srcBlur[0];
	cannyCPUSoloGPUStruct.canny = &canny[0];
	cannyCPUSoloGPUStruct.threadId = 0;

	pthread_t cannyCPUSoloGPUThread2;
	struct CannyGPUStruct cannyCPUSoloGPUStruct2;
	cannyCPUSoloGPUStruct2.numOfIteration = (int) ((float) totNumIteration
			* 0.30f/*gpuLoad*/);
	cannyCPUSoloGPUStruct2.gpuSrc = &gpuSrc;
	cannyCPUSoloGPUStruct2.gpuSrcGray = &gpuSrcGray;
	cannyCPUSoloGPUStruct2.gpuSrcBlur = &gpuSrcBlur;
	cannyCPUSoloGPUStruct2.gpuCanny = &gpuCanny;
	cannyCPUSoloGPUStruct2.ced = &ced;
	cannyCPUSoloGPUStruct2.filter = &filter;
	cannyCPUSoloGPUStruct2.threadId = 1;

	auto startCPUSoloGPUHybrid = std::chrono::system_clock::now();

	/* create the threads which executes canny edge detection in cpu */

	if (pthread_create(&cannyCPUSoloGPUThread, &cPU_t_attr, cannyCPU,
					(void*) &cannyCPUSoloGPUStruct)) {
		printf("Error creating thread[%i]!\n", cannyCPUSoloGPUStruct.threadId);
		return 1;
	}

	if (pthread_create(&cannyCPUSoloGPUThread2, &gPU_t_attr, cannyGPU,
					(void*) &cannyCPUSoloGPUStruct2)) {
		printf("Error creating thread[%i]!\n", cannyCPUSoloGPUStruct2.threadId);
		return 1;
	}
	/* wait for the threads to finish */
	if (pthread_join(cannyCPUSoloGPUThread2, NULL)) {
		printf("Error joining thread[%i]!\n", 1);
		return 2;
	}
	auto finishGPU = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsedTimeGPU = finishGPU
	- startCPUSoloGPUHybrid;
	printf("GPU Elapsed Time: %f msecs\n", elapsedTimeGPU* 1000);
	/* wait for the threads to finish */
	if (pthread_join(cannyCPUSoloGPUThread, NULL)) {
		printf("Error joining thread[%i]!\n", 0);
		return 2;
	}

	auto finishCPUSoloGPUMulti = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsedTimeCPUSoloGPUMulti =
	finishCPUSoloGPUMulti - startCPUSoloGPUHybrid;
	printf("CPUSoloGPU Hybrid Elapsed Time: %f msecs\n",
			elapsedTimeCPUSoloGPUMulti.count() * 1000);
	printf("Time improvement= %f %\n",
			(elapsedTimeCPUSolo.count() - elapsedTimeCPUSoloGPUMulti.count())
			/ elapsedTimeCPUSolo.count() * 100);

	/**
	 * Hybrids 3 cpu + 1 gpu (Parent allocate the gpu)
	 * Ratio for caanny edge detection in tx2 is 0.46 inst GPU and 0.56 int CPU
	 */
	pthread_t cannyCPUGPUThread[totNumThread - 1];
	struct CannyCPUStruct cannyCPUGPUStruct[totNumThread - 1];
	for (int t = 0; t < totNumThread - 1; ++t) {
		cannyCPUGPUStruct[t].numOfIteration = (int) ((float) totNumIteration
				* 0.80f) / (totNumThread - 1);
		cannyCPUGPUStruct[t].src = &src;
		cannyCPUGPUStruct[t].srcGray = &srcGray[t];
		cannyCPUGPUStruct[t].srcBlur = &srcBlur[t];
		cannyCPUGPUStruct[t].canny = &canny[t];
		cannyCPUGPUStruct[t].threadId = t;
	}

	pthread_t cannyCPUGPUThread2;
	struct CannyGPUStruct cannyCPUGPUStruct2;
	cannyCPUGPUStruct2.numOfIteration =
	(int) ((float) totNumIteration * 0.20f/*gpuLoad*/);
	cannyCPUGPUStruct2.gpuSrc = &gpuSrc;
	cannyCPUGPUStruct2.gpuSrcGray = &gpuSrcGray;
	cannyCPUGPUStruct2.gpuSrcBlur = &gpuSrcBlur;
	cannyCPUGPUStruct2.gpuCanny = &gpuCanny;
	cannyCPUGPUStruct2.ced = &ced;
	cannyCPUGPUStruct2.filter = &filter;
	cannyCPUGPUStruct2.threadId = totNumThread - 1;

	auto startCPUGPUHybrid = std::chrono::system_clock::now();

	/* create the threads which executes canny edge detection in cpu */
	for (int t = 0; t < totNumThread - 1; ++t) {
		if (pthread_create(&cannyCPUGPUThread[t], &cPU_t_attr, cannyCPU,
						(void*) &cannyCPUGPUStruct[t])) {
			printf("Error creating thread[%i]!\n",
					cannyCPUGPUStruct[t].threadId);
			return 1;
		}
	}
	if (pthread_create(&cannyCPUGPUThread2, &gPU_t_attr, cannyGPU,
					(void*) &cannyCPUGPUStruct2)) {
		printf("Error creating thread[%i]!\n", cannyCPUGPUStruct2.threadId);
		return 1;
	}
	if (pthread_join(cannyCPUGPUThread2, NULL)) {
		printf("Error joining thread[%i]!\n", cannyCPUGPUStruct2.threadId);
		return 2;
	}
	auto finishGPU2 = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsedTimeGPU2 = finishGPU2
	- startCPUGPUHybrid;
	printf("GPU Elapsed Time: %f msecs\n", elapsedTimeGPU2* 1000);
	/* wait for the threads to finish */
	for (int t = 0; t < totNumThread - 1; ++t) {
		if (pthread_join(cannyCPUGPUThread[t], NULL)) {
			printf("Error joining thread[%i]!\n",
					cannyCPUGPUStruct[t].threadId);
			return 2;
		}
	}

	auto finishCPUGPUHybrid = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsedTimeCPUGPUHybrid = finishCPUGPUHybrid
	- startCPUGPUHybrid;
	printf("CPUGPU Hybrid Elapsed Time: %f msecs\n",
			elapsedTimeCPUGPUHybrid.count() * 1000);
	printf("Time improvement= %f %\n",
			(elapsedTimeCPUSolo.count() - elapsedTimeCPUGPUHybrid.count())
			/ elapsedTimeCPUSolo.count() * 100);

	/**
	 * Hybrids 4 cpu + 1 cpu + 1 gpu (One more thread.)
	 * This is to test whether we were able to context switch fast enough
	 * Ratio for canny edge detection in tx2 is 0.46 inst GPU and 0.56 int CPU
	 */

	pthread_t cannyCPUMultiGPUThread[totNumThread];
	struct CannyCPUStruct cannyCPUMultiGPUStruct[totNumThread];
	for (int t = 0; t < totNumThread; ++t) {
		cannyCPUMultiGPUStruct[t].numOfIteration =
		(int) ((float) totNumIteration * cpuLoad) / (totNumThread);
		cannyCPUMultiGPUStruct[t].src = &src;
		cannyCPUMultiGPUStruct[t].srcGray = &srcGray[t];
		cannyCPUMultiGPUStruct[t].srcBlur = &srcBlur[t];
		cannyCPUMultiGPUStruct[t].canny = &canny[t];
		cannyCPUMultiGPUStruct[t].threadId = t;
	}
	pthread_t cannyCPUMultiGPUThread2;
	struct CannyGPUStruct cannyCPUMultiGPUStruct2;
	cannyCPUMultiGPUStruct2.numOfIteration = (int) ((float) totNumIteration
			* gpuLoad);
	cannyCPUMultiGPUStruct2.gpuSrc = &gpuSrc;
	cannyCPUMultiGPUStruct2.gpuSrcGray = &gpuSrcGray;
	cannyCPUMultiGPUStruct2.gpuSrcBlur = &gpuSrcBlur;
	cannyCPUMultiGPUStruct2.gpuCanny = &gpuCanny;
	cannyCPUMultiGPUStruct2.threadId = totNumThread;
	cannyCPUMultiGPUStruct2.ced = &ced;
	cannyCPUMultiGPUStruct2.filter = &filter;

	auto startCPUMultiGPUHybrid = std::chrono::system_clock::now();

	/* create the threads which executes canny edge detection in cpu */
	for (int t = 0; t < totNumThread; ++t) {
		if (pthread_create(&cannyCPUMultiGPUThread[t], &cPU_t_attr, cannyCPU,
						(void*) &cannyCPUMultiGPUStruct[t])) {
			printf("Error creating thread[%i]!\n",
					cannyCPUMultiGPUStruct[t].threadId);
			return 1;
		}
	}
	/* create the threads which executes canny edge detection in gpu */
	if (pthread_create(&cannyCPUMultiGPUThread2, &gPU_t_attr, cannyGPU,
					(void*) &cannyCPUMultiGPUStruct2)) {
		printf("Error creating thread[i%]!\n",
				cannyCPUMultiGPUStruct2.threadId);
		return 1;
	}

	if (pthread_join(cannyCPUMultiGPUThread2, NULL)) {
		printf("Error joining thread[%i]!\n", cannyCPUMultiGPUStruct2.threadId);
		return 2;
	}
	auto finishGPU3 = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsedTimeGPU3 = finishGPU3
	- startCPUMultiGPUHybrid;
	printf("GPU Elapsed Time: %f msecs\n", elapsedTimeGPU3* 1000);
	/* wait for the threads to finish */
	for (int t = 0; t < totNumThread; ++t) {
		if (pthread_join(cannyCPUMultiGPUThread[t], NULL)) {
			printf("Error joining thread[%i]!\n",
					cannyCPUMultiGPUStruct[t].threadId);
			return 2;
		}
	}

	auto finishCPUMultiGPUHybrid = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsedTimeCPUMultiGPUHybrid =
	finishCPUMultiGPUHybrid - startCPUMultiGPUHybrid;
	printf("CPUGPU Multi Elapsed Time: %f msecs\n",
			elapsedTimeCPUMultiGPUHybrid.count() * 1000);
	printf("Time improvement= %f %\n",
			(elapsedTimeCPUSolo.count() - elapsedTimeCPUMultiGPUHybrid.count())
			/ elapsedTimeCPUSolo.count() * 100);
#endif
	printf("Done!\n");
//	gpuCanny.download(src[0]);

//	cv::namedWindow("CPU Canny Edge", cv::WINDOW_AUTOSIZE);
//	cv::imshow("CPU Canny Edge", canny[0]);
//	cv::namedWindow("GPU Canny Edge", cv::WINDOW_AUTOSIZE);
//	cv::imshow("GPU Canny Edge", src[0]);
//	cv::waitKey(0);
	return (0);
}
