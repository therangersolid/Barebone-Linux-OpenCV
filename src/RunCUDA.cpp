///*------------------------------------------------------*
// * RunCUDA.cpp                                          *
// * OpenCV's Hello World in CUDA                         *
// *------------------------------------------------------*/
//
///**
// * This is for the definition of cv::mat
// */
//#include "opencv2/core/core.hpp"
//
///**
// * This is for the imread function
// */
//#include "opencv2/imgcodecs.hpp"
//
///**
// * Image processing functions with the cvtColor
// */
//#include "opencv2/imgproc/imgproc.hpp"
//
///**
// * CUDA Counterparts:
// */
//#include "opencv2/core/cuda.hpp"
//#include "opencv2/cudaimgproc.hpp"
//
///**
// * Display the window with results
// */
//#include "opencv2/highgui/highgui.hpp"
//#include <stdio.h>
//
//cv::Mat src;
//cv::Mat src_gray;
//
//cv::cuda::GpuMat gpuSrc;
//cv::cuda::GpuMat gpuSrc_gray;
//cv::Mat gpuSrc_host;
//
//
//int main(int, char** argv) {
//	printf("Device Count = %i\n", cv::cuda::getCudaEnabledDeviceCount());;
//	src = cv::imread("stop-2018-07-03.png");
//
//	// CPU part:
//	cvtColor(src, src_gray, cv::COLOR_BGR2GRAY);
//
//	const char* source_window = "SourceCPU";
//	namedWindow(source_window, cv::WINDOW_AUTOSIZE);
//	imshow(source_window, src_gray);
//	/////////////////////////////////////////////////////////////
//	// GPU part:
//	gpuSrc.upload(src);
//
//	cv::cuda::cvtColor(gpuSrc, gpuSrc_gray, cv::COLOR_BGR2GRAY); //cvtColor(gpuSrc, gpuSrc_gray, cv::COLOR_BGR2GRAY);
//	gpuSrc_gray.download(gpuSrc_host);
//
//	const char* source_window1 = "SourceGPUCUDA";
//	namedWindow(source_window1, cv::WINDOW_AUTOSIZE);
//	imshow(source_window1, gpuSrc_host);
//	//////////////////////////////////////////////////////////////
//	cv::waitKey(0);
//	return (0);
//
//}
