///*------------------------------------------------------*
// * Code: moments.cpp                                    *
// * Coded by:  opencv tutorial;                          *
// *       Computer Vision and AI Training Course         *
// *       for course/program contact Harry Li            *
// * Date: June 26, 2018;                                 *
// * Version: x0.1;                                       *
// * Status : release;                                    *
// * Purpose: 1. extraction of moments features           *
// *------------------------------------------------------*/
//
///**
// * This is for the definition of cv::mat
// */
//#include "opencv2/core/core.hpp"
//
///**
// * This is for the imread function
// */
//#include "opencv2/imgcodecs.hpp"
//
///**
// * Image processing functions with the cvtColor
// */
//#include "opencv2/imgproc/imgproc.hpp"
//
///**
// * Display the window with results
// */
//#include "opencv2/highgui/highgui.hpp"
//
///**
// * CUDA Counterparts:
// */
//#include "opencv2/core/cuda.hpp"
//#include "opencv2/cudaimgproc.hpp"
//#include "opencv2/cudafilters.hpp"
//
//#include <stdio.h>
//#include <chrono>
//
//cv::Mat src;
//cv::Mat srcGray;
//cv::Mat srcBlur;
//cv::Mat canny;
//
//cv::cuda::GpuMat gpuSrc;
//cv::cuda::GpuMat gpuSrcGray;
//cv::cuda::GpuMat gpuSrcBlur;
//cv::cuda::GpuMat gpuCanny;
//
////#define thresh 100
//int thresh = 100;
//#define max_thresh 255
//int i = 0;
//
//int main(int, char** argv) {
//	/**
//	 * CPU Parts
//	 */
//	src = cv::imread("stop-2018-07-03.png");
//	auto start = std::chrono::high_resolution_clock::now();
//	for (i = 0; i < 10000; ++i) {
//		cv::cvtColor(src, srcGray, cv::COLOR_BGR2GRAY);
//		cv::GaussianBlur( srcGray, srcBlur, cv::Size(3,3), 0,0);
//		cv::Canny(srcBlur, canny, thresh, thresh * 2, 3);
//	}
//	auto finish = std::chrono::high_resolution_clock::now();
//	std::chrono::duration<double> elapsedTime = finish - start;
//	printf("Elapsed Time: %f msecs\n", elapsedTime.count() * 1000);
//
//	/**
//	 * GPU Parts
//	 */
//	gpuSrc.upload(src);
//	cv::Ptr<cv::cuda::CannyEdgeDetector> ced =
//					cv::cuda::createCannyEdgeDetector(thresh, thresh * 2, 3);
//	cv::Ptr<cv::cuda::Filter> filter = cv::cuda::createGaussianFilter(gpuSrcGray.type(), gpuSrcBlur.type(), cv::Size(3,3), 0, 0);
//	auto startGPU = std::chrono::high_resolution_clock::now();
//	for (i = 0; i < 10000; ++i) {
//		cv::cuda::cvtColor(gpuSrc, gpuSrcGray, cv::COLOR_BGR2GRAY);
//		filter->apply(gpuSrcGray, gpuSrcBlur);
//		ced->detect(gpuSrcBlur, gpuCanny);
//	}
//	auto finishGPU = std::chrono::high_resolution_clock::now();
//	std::chrono::duration<double> elapsedTimeGPU = finishGPU - startGPU;
//	printf("Elapsed Time: %f msecs\n", elapsedTimeGPU.count() * 1000);
//	gpuCanny.download(src);
//
//	cv::namedWindow("CPU Contours", cv::WINDOW_AUTOSIZE);
//	cv::imshow("CPU Contours", canny);
//	cv::namedWindow("GPU Contours", cv::WINDOW_AUTOSIZE);
//	cv::imshow("GPU Contours", src);
//	cv::waitKey(0);
//	return (0);
//}
