///*------------------------------------------------------*
// * RunOpenCL.cpp                                        *
// * OpenCV's Hello World in OpenCL                       *
// *------------------------------------------------------*/
//
///**
// * This is for the definition of cv::mat
// */
//#include "opencv2/core/core.hpp"
//
///**
// * This is for the imread function
// */
//#include "opencv2/imgcodecs.hpp"
//
///**
// * Image processing functions with the cvtColor
// */
//#include "opencv2/imgproc/imgproc.hpp"
//
///**
// * OpenGL Counterparts:
// */
////#include "opencv2/core/opengl.hpp"
///**
// * OpenCL Counterparts:
// */
//#include "opencv2/core/ocl.hpp"
//
///**
// * Display the window with results
// */
//#include "opencv2/highgui/highgui.hpp"
//#include <stdio.h>
//
//cv::UMat oclSrc;
//cv::UMat oclSrc_gray;
//
//int main(int, char** argv) {
//	cv::ocl::Context context;
//	context.create(cv::ocl::Device::TYPE_ALL);
//	printf("Number of device = %i\n", context.ndevices());
//	cv::ocl::Device device;
//	std::string deviceName;
//	for (int i = 0; i < context.ndevices(); i++) {
//		device = context.device(i);
//		deviceName = device.name();
//		printf("Device [%i]=\n", i);
//		printf(deviceName.c_str());
//		printf("\n");
//	}
//	cv::imread("stop-2018-07-03.png").copyTo(oclSrc);
//
//	// OpenCL part:
//	cv::cvtColor(oclSrc, oclSrc_gray, cv::COLOR_BGR2GRAY);
//
//	const char* source_window1 = "SourceOCL";
//	namedWindow(source_window1, cv::WINDOW_AUTOSIZE);
//	imshow(source_window1, oclSrc_gray);
//
//	cv::waitKey(0);
//	return (0);
//
//}
