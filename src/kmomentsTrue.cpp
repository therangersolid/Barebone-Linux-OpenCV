//// Add for camera input
//#include "opencv2/opencv.hpp"
//
//// This is for the definition of cv::mat
//#include "opencv2/core/core.hpp"
//
//// This is for the imread function
//#include "opencv2/imgcodecs.hpp"
//
//// Image processing functions with the cvtColor
//#include "opencv2/imgproc/imgproc.hpp"
//
//// Display the window with results
//#include "opencv2/highgui/highgui.hpp"
//
//// CUDA Counterparts:
//#include "opencv2/core/cuda.hpp"
//
//// Image processing functions with the cuda::cvtColor
//#include "opencv2/cudaimgproc.hpp"
//
//// This is for implementing the gaussian blur for cuda
//#include "opencv2/cudafilters.hpp"
//
//// Pthread for the multithreading
//#include <pthread.h>
//
//#include <stdio.h>
//
//// Chrono is for timer
//#include <chrono>
//
//// This is for passing to pthread as arguments
//struct CannyCPUStruct {
//	int threadId;
//	int numOfIteration;
//	cv::Mat* src;
//	cv::Mat* srcGray;
//	cv::Mat* srcBlur;
//	cv::Mat* canny;
//};
//
//// This is for passing to pthread as arguments
//struct CannyGPUStruct {
//	int threadId;
//	int numOfIteration;
//	cv::cuda::GpuMat* gpuSrc;
//	cv::cuda::GpuMat* gpuSrcGray;
//	cv::cuda::GpuMat* gpuSrcBlur;
//	cv::cuda::GpuMat* gpuCanny;
//	cv::Ptr<cv::cuda::CannyEdgeDetector>* ced;
//	cv::Ptr<cv::cuda::Filter>* filter;
//
//};
//
//#define totNumIteration 2000
//#define totNumThread 4 // Including one parent.
//int thresh = 100;
//#define max_thresh 255
//
///**
// * Note on opencv Mat operation:
// * Mat A = imread(argv[1], CV_LOAD_IMAGE_COLOR);
//
// * Mat B = A.clone();  // B is a deep copy of A. (has its own copy of the pixels)
// * Mat C = A;          // C is a shallow copy of A ( rows, cols copied, but shared pixel-pointer )
// * Mat D; A.copyTo(D); // D is a deep copy of A, like B
// */
//
///* The threads */
//void *cannyCPU(void *arguments){
//	struct CannyCPUStruct* cannyCPUStruct= (struct CannyCPUStruct*)arguments;
//	cv::Mat src = *(cannyCPUStruct->src); // shallow copy
//	cv::Mat srcGray = *(cannyCPUStruct->srcGray); // shallow copy
//	cv::Mat srcBlur=*(cannyCPUStruct->srcBlur); // shallow copy
//	cv::Mat canny=*(cannyCPUStruct->canny); // shallow copy
//	int numOfIteration = cannyCPUStruct->numOfIteration;
//	for (int i = 0; i < numOfIteration; ++i) {
//		cv::cvtColor(src, srcGray, cv::COLOR_BGR2GRAY);
//		cv::GaussianBlur(srcGray, srcBlur, cv::Size(3, 3), 0, 0);
//		cv::Canny(srcGray, canny, thresh, thresh * 2, 3);
//	}
//	printf("CPU[%i] Done!\n",cannyCPUStruct->threadId);
//	/* the function must return something - NULL will do */
//	return NULL;
//}
//
//void *cannyGPU(void *arguments){
//	struct CannyGPUStruct* cannyGPUStruct= (struct CannyGPUStruct*)arguments;
//	cv::cuda::GpuMat gpuSrc = *(cannyGPUStruct->gpuSrc); // shallow copy
//	cv::cuda::GpuMat gpuSrcGray = *(cannyGPUStruct->gpuSrcGray); // shallow copy
//	cv::cuda::GpuMat gpuSrcBlur=*(cannyGPUStruct->gpuSrcBlur); // shallow copy
//	cv::cuda::GpuMat gpuCanny=*(cannyGPUStruct->gpuCanny); // shallow copy
//	cv::Ptr<cv::cuda::CannyEdgeDetector> ced = *(cannyGPUStruct->ced);
//	cv::Ptr<cv::cuda::Filter> filter = *(cannyGPUStruct->filter);
//	int numOfIteration = cannyGPUStruct->numOfIteration;
//	for (int i = 0; i < numOfIteration; ++i) {
//		cv::cuda::cvtColor(gpuSrc, gpuSrcGray, cv::COLOR_BGR2GRAY);
//		filter->apply(gpuSrcGray, gpuSrcBlur);
//		ced->detect(gpuSrcBlur, gpuCanny);
//	}
//	printf("GPU[%i] Done!\n",cannyGPUStruct->threadId);
//	/* the function must return something - NULL will do */
//	return NULL;
//}
//
//int main(int, char** argv) {
//	cv::Mat img;
//	// Data required for this test:
//	cv::Mat src[totNumThread];
//	cv::Mat srcGray[totNumThread];
//	cv::Mat srcBlur[totNumThread];
//	cv::Mat canny[totNumThread];
//
//	cv::cuda::GpuMat gpuSrc[totNumThread];
//	cv::cuda::GpuMat gpuSrcGray[totNumThread];
//	cv::cuda::GpuMat gpuSrcBlur[totNumThread];
//	cv::cuda::GpuMat gpuCanny[totNumThread];
//
//	cv::Ptr<cv::cuda::CannyEdgeDetector> ced[totNumThread];
//	cv::Ptr<cv::cuda::Filter> filter[totNumThread];
//
////	cv::VideoCapture cap("nvcamerasrc ! video/x-raw(memory:NVMM), width=(int)1280, height=(int)720, format=(string)I420, framerate=(fraction)120/1 ! \
////                    nvvidconv   ! video/x-raw, format=(string)BGRx ! \
////                    videoconvert ! video/x-raw, format=(string)BGR ! \
////                    appsink");
////
////	if (!cap.isOpened()) {
////		return -1;
////	}
////	cv::namedWindow("Camera", cv::WINDOW_AUTOSIZE);
////	for (i = 0; i < 10; ++i) {
////		cap >> src;
////		cv::imshow("Camera", src);
////	}
//	img = cv::imread("nvidia-logo.jpg");
//	for(int t = 0; t < totNumThread; ++t){
//		// Preparation:
//		src[t] = img.clone();
//		// Skip 1st call gpu api because 1st call might consume lots of time
//		gpuSrc[t].upload(src[t]);
//		// CED & Filter
//		ced[t] = cv::cuda::createCannyEdgeDetector(thresh, thresh * 2, 3);
//		filter[t] = cv::cuda::createGaussianFilter(
//			gpuSrcGray[t].type(), gpuSrcBlur[t].type(), cv::Size(3, 3), 0, 0);
//
//		cv::cuda::cvtColor(gpuSrc[t], gpuSrcGray[t], cv::COLOR_BGR2GRAY);
//		filter[t]->apply(gpuSrcGray[t], gpuSrcBlur[t]);
//		ced[t]->detect(gpuSrcBlur[t], gpuCanny[t]);
//	}
//	/**
//	 * CPU Parts Single threaded
//	 */
//
//	auto start = std::chrono::system_clock::now();
//	for (int i = 0; i < totNumIteration; ++i) {
//		cv::cvtColor(src[0], srcGray[0], cv::COLOR_BGR2GRAY);
//		cv::GaussianBlur(srcGray[0], srcBlur[0], cv::Size(3, 3), 0, 0);
//		cv::Canny(srcGray[0], canny[0], thresh, thresh * 2, 3);
//	}
//	auto finish = std::chrono::system_clock::now();
//	std::chrono::duration<double> elapsedTime = finish - start;
//	printf("CPU Single Elapsed Time: %f msecs\n", elapsedTime.count() * 1000);
//
//	/**
//	 * CPU Parts 4 thread (Including one parent. Parent is 0, and thread0 is 1, etc)
//	 */
//	pthread_t cannyCPUThread[totNumThread-1];
//	struct CannyCPUStruct cannyCPUStruct[totNumThread-1];
//	for(int t = 0; t < totNumThread-1; ++t){
//		cannyCPUStruct[t].numOfIteration = totNumIteration/totNumThread;
//		cannyCPUStruct[t].src=&src[t+1];
//		cannyCPUStruct[t].srcGray=&srcGray[t+1];
//		cannyCPUStruct[t].srcBlur=&srcBlur[t+1];
//		cannyCPUStruct[t].canny=&canny[t+1];
//		cannyCPUStruct[t].threadId=t+1;
//	}
//	auto startCPUMulti = std::chrono::system_clock::now();
//
//	/* create the threads which executes canny edge detection in cpu */
//	for(int t = 0; t < totNumThread-1; ++t){
//		if(pthread_create(&cannyCPUThread[t], NULL, cannyCPU, (void* )&cannyCPUStruct[t])) {
//			printf("Error creating thread[%i]!\n",t);
//		return 1;
//		}
//	}
//	// Parent Calculations:
//	for (int i = 0; i < totNumIteration/totNumThread; ++i) {
//		cv::cvtColor(src[0], srcGray[0], cv::COLOR_BGR2GRAY);
//		cv::GaussianBlur(srcGray[0], srcBlur[0], cv::Size(3, 3), 0, 0);
//		cv::Canny(srcGray[0], canny[0], thresh, thresh * 2, 3);
//	}
//	printf("CPU[0] Done!\n");
//	/* wait for the threads to finish */
//	for(int t = 0; t < totNumThread-1; ++t){
//		if(pthread_join(cannyCPUThread[t], NULL)) {
//			printf("Error joining thread[%i]!\n",t);
//		return 2;
//		}
//	}
//	auto finishCPUMulti = std::chrono::system_clock::now();
//	std::chrono::duration<double> elapsedTimeCPUMulti = finishCPUMulti - startCPUMulti;
//	printf("CPU Multi Elapsed Time: %f msecs\n", elapsedTimeCPUMulti.count() * 1000);
//
//	/**
//	 * GPU Single Parts
//	 */
//
//	auto startGPU = std::chrono::system_clock::now();
//	for (int i = 0; i < totNumIteration; ++i) {
//		cv::cuda::cvtColor(gpuSrc[0], gpuSrcGray[0], cv::COLOR_BGR2GRAY);
//		filter[0]->apply(gpuSrcGray[0], gpuSrcBlur[0]);
//		ced[0]->detect(gpuSrcBlur[0], gpuCanny[0]);
//	}
//	auto finishGPU = std::chrono::system_clock::now();
//	std::chrono::duration<double> elapsedTimeGPU = finishGPU - startGPU;
//	printf("GPU Single Elapsed Time: %f msecs\n", elapsedTimeGPU.count() * 1000);
//
//	/**
//	 * GPU Multi Parts
//	 */
//	pthread_t cannyGPUThread[totNumThread-1];
//	struct CannyGPUStruct cannyGPUStruct[totNumThread-1];
//	for(int t = 0; t < totNumThread-1; ++t){
//		cannyGPUStruct[t].numOfIteration = totNumIteration/totNumThread;
//		cannyGPUStruct[t].gpuSrc=&gpuSrc[t+1];
//		cannyGPUStruct[t].gpuSrcGray=&gpuSrcGray[t+1];
//		cannyGPUStruct[t].gpuSrcBlur=&gpuSrcBlur[t+1];
//		cannyGPUStruct[t].gpuCanny=&gpuCanny[t+1];
//		cannyGPUStruct[t].ced = &ced[t+1];
//		cannyGPUStruct[t].filter = &filter[t+1];
//		cannyGPUStruct[t].threadId=t+1;
//	}
//	auto startGPUMulti = std::chrono::system_clock::now();
//
//	/* create the threads which executes canny edge detection in gpu */
//	for(int t = 0; t < totNumThread-1; ++t){
//		if(pthread_create(&cannyGPUThread[t], NULL, cannyGPU, (void* )&cannyGPUStruct[t])) {
//			printf("Error creating thread[%i]!\n",t);
//		return 1;
//		}
//	}
//	// Parent Calculations:
//	for (int i = 0; i < totNumIteration/totNumThread; ++i) {
//		cv::cuda::cvtColor(gpuSrc[0], gpuSrcGray[0], cv::COLOR_BGR2GRAY);
//		filter[0]->apply(gpuSrcGray[0], gpuSrcBlur[0]);
//		ced[0]->detect(gpuSrcBlur[0], gpuCanny[0]);
//	}
//	printf("GPU[0] Done!\n");
//	/* wait for the threads to finish */
//	for(int t = 0; t < totNumThread-1; ++t){
//		if(pthread_join(cannyGPUThread[t], NULL)) {
//			printf("Error joining thread[%i]!\n",t);
//		return 2;
//		}
//	}
//	auto finishGPUMulti = std::chrono::system_clock::now();
//	std::chrono::duration<double> elapsedTimeGPUMulti = finishGPUMulti - startGPUMulti;
//	printf("GPU Multi Elapsed Time: %f msecs\n", elapsedTimeGPUMulti.count() * 1000);
//
//	/**
//	 * Hybrids 4 cpu + 1 gpu (Parent allocate the gpu)
//	 * Ratio for caanny edge detection in tx2 is 0.46 inst GPU and 0.56 int CPU
//	 */
//	pthread_t cannyCPUGPUThread[totNumThread-1];
//	struct CannyCPUStruct cannyCPUGPUStruct[totNumThread-1];
//	for(int t = 0; t < totNumThread-1; ++t){
//		cannyCPUGPUStruct[t].numOfIteration = (int)((float)totNumIteration*0.54f)/(totNumThread-1);
//		cannyCPUGPUStruct[t].src=&src[t+1];
//		cannyCPUGPUStruct[t].srcGray=&srcGray[t+1];
//		cannyCPUGPUStruct[t].srcBlur=&srcBlur[t+1];
//		cannyCPUGPUStruct[t].canny=&canny[t+1];
//		cannyCPUGPUStruct[t].threadId=t+1;
//	}
//	auto startCPUGPUMulti = std::chrono::system_clock::now();
//
//	/* create the threads which executes canny edge detection in cpu */
//	for(int t = 0; t < totNumThread-1; ++t){
//		if(pthread_create(&cannyCPUGPUThread[t], NULL, cannyCPU, (void* )&cannyCPUGPUStruct[t])) {
//			printf("Error creating thread[%i]!\n",t);
//		return 1;
//		}
//	}
//	// Parent Calculations:
//	for (int i = 0; i < (int)(totNumIteration*0.46); ++i) {
//		cv::cuda::cvtColor(gpuSrc[0], gpuSrcGray[0], cv::COLOR_BGR2GRAY);
//		filter[0]->apply(gpuSrcGray[0], gpuSrcBlur[0]);
//		ced[0]->detect(gpuSrcBlur[0], gpuCanny[0]);
//	}
//	printf("GPU[0] Done!\n");
//	auto finishGPU0Multi = std::chrono::system_clock::now();
//	std::chrono::duration<double> elapsedTimeGPU0Multi = finishGPU0Multi - startCPUGPUMulti;
//	printf("GPU[0] Elapsed Time: %f msecs\n", elapsedTimeGPU0Multi.count() * 1000);
//
//	/* wait for the threads to finish */
//	for(int t = 0; t < totNumThread-1; ++t){
//		if(pthread_join(cannyCPUGPUThread[t], NULL)) {
//			printf("Error joining thread[%i]!\n",t);
//		return 2;
//		}
//	}
//	auto finishCPUGPUMulti = std::chrono::system_clock::now();
//	std::chrono::duration<double> elapsedTimeCPUGPUMulti = finishCPUGPUMulti - startCPUGPUMulti;
//	printf("CPUGPU Multi Elapsed Time: %f msecs\n", elapsedTimeCPUGPUMulti.count() * 1000);
//
//
//	gpuCanny[0].download(src[0]);
//
////	cv::namedWindow("CPU Canny Edge", cv::WINDOW_AUTOSIZE);
////	cv::imshow("CPU Canny Edge", canny[0]);
////	cv::namedWindow("GPU Canny Edge", cv::WINDOW_AUTOSIZE);
////	cv::imshow("GPU Canny Edge", src[0]);
////	cv::waitKey(0);
//	return (0);
//}
