//// Add for camera input
//#include "opencv2/opencv.hpp"
//
//// This is for the definition of cv::mat
//#include "opencv2/core/core.hpp"
//
//// This is for the imread function
//#include "opencv2/imgcodecs.hpp"
//
//// Image processing functions with the cvtColor
//#include "opencv2/imgproc/imgproc.hpp"
//
//// Display the window with results
//#include "opencv2/highgui/highgui.hpp"
//
//#include <stdio.h>
//
//#include <chrono>
//
//#include <vector>
//
//// Timers:
////#include <signal.h>
////#include <sys/time.h>
////#include <string.h>
////
////volatile int done;
////void timer_handler(int signum) {
////	done = 1;
////}
//
//int main(int, char** argv) {
//	// Initializers;
////	done = 0;
//
/////////////////////////////
//// Show all available camera:
//	printf("===============All camera:================\n");
//	std::vector<std::string*> cameras;
//	int strSize = 200;
//	FILE* pipe = popen("ls /dev/video*", "r");
//	if (!pipe) {
//		printf("popen() failed!");
//		return 1;
//	}
//	char buffer[200];	// buffer is 100 chars in length
//	while (fgets(buffer, sizeof(char) * strSize, pipe) != NULL) {
//		std::string* stringBuffer = new std::string;
//		stringBuffer->append(buffer);
//		stringBuffer->pop_back();	// There is a \n included here! so pop it!
//		cameras.push_back(stringBuffer);
//		printf("%s", stringBuffer->c_str());
//	}
//	pclose(pipe);
//	for (int i = 0; i < cameras.size(); ++i) {
//		std::string stringBuffer;
//		stringBuffer.append("udevadm info --query=all --name=");
//		stringBuffer.append(cameras.at(i)->c_str());
//		pipe = popen(stringBuffer.c_str(), "r");
//		if (!pipe) {
//			printf("popen() failed!");
//			return 1;
//		}
//		while (fgets(buffer, sizeof(char) * strSize, pipe) != NULL) {
//			printf("%s", buffer);
//		}
//		pclose(pipe);
//	}
//
//	printf("Make sure to check with the v4l2-ctl --list-devices\n");
//	printf("install by using sudo apt-get install v4l-utils\n");
//
//	///////////////////////////
//	cv::Mat src;
//	cv::VideoCapture cap;
//	cap.open(*cameras.at(0));
//
////    				cap = cv::VideoCapture(
////					"rtsp://admin:admin123@192.168.1.62:554/Streaming/channels/1"); // 30fps
////			cap = cv::VideoCapture(
////					"rtsp://admin:admin123@192.168.1.66:554/Streaming/channels/1"); // 60fps
//	if (!cap.isOpened()) {
//		printf("No camera is connected!!");
//		return 0;
//	}
//	printf("===============Camera specs:================\n");
//	printf("FPS = %f\n", cap.get(cv::CAP_PROP_FPS));
//	printf("Width x Height = %fx%f\n", cap.get(cv::CAP_PROP_FRAME_WIDTH),
//			cap.get(cv::CAP_PROP_FRAME_HEIGHT));
//	printf("%i,%f\n", cv::CAP_PROP_BRIGHTNESS,
//			cap.get(cv::CAP_PROP_BRIGHTNESS)); //!< Brightness of the image (only for those cameras that support).
//	printf("%i,%f\n", cv::CAP_PROP_CONTRAST, cap.get(cv::CAP_PROP_CONTRAST)); //!< Contrast of the image (only for cameras).
//	printf("%i,%f\n", cv::CAP_PROP_SATURATION,
//			cap.get(cv::CAP_PROP_SATURATION)); //!< Saturation of the image (only for cameras).
//	printf("%i,%f\n", cv::CAP_PROP_HUE, cap.get(cv::CAP_PROP_HUE)); //!< Hue of the image (only for cameras).
//	printf("%i,%f\n", cv::CAP_PROP_GAIN, cap.get(cv::CAP_PROP_GAIN)); //!< Gain of the image (only for those cameras that support).
//	printf("%i,%f\n", cv::CAP_PROP_EXPOSURE, cap.get(cv::CAP_PROP_EXPOSURE)); //!
//	printf("%i,%f\n", cv::CAP_PROP_MONOCHROME,
//			cap.get(cv::CAP_PROP_MONOCHROME));
//	printf("%i,%f\n", cv::CAP_PROP_SHARPNESS, cap.get(cv::CAP_PROP_SHARPNESS));
//	printf("%i,%f\n", cv::CAP_PROP_AUTO_EXPOSURE,
//			cap.get(cv::CAP_PROP_AUTO_EXPOSURE)); //!< DC1394: exposure control done by camera, user can adjust reference level using this feature.
//	printf("%i,%f\n", cv::CAP_PROP_GAMMA, cap.get(cv::CAP_PROP_GAMMA));
//	printf("%i,%f\n", cv::CAP_PROP_TEMPERATURE,
//			cap.get(cv::CAP_PROP_TEMPERATURE));
//	printf("%i,%f\n", cv::CAP_PROP_TRIGGER, cap.get(cv::CAP_PROP_TRIGGER));
//	printf("%i,%f\n", cv::CAP_PROP_TRIGGER_DELAY,
//			cap.get(cv::CAP_PROP_TRIGGER_DELAY));
//	printf("%i,%f\n", cv::CAP_PROP_WHITE_BALANCE_RED_V,
//			cap.get(cv::CAP_PROP_WHITE_BALANCE_RED_V));
//	printf("%i,%f\n", cv::CAP_PROP_ZOOM, cap.get(cv::CAP_PROP_ZOOM));
//	printf("%i,%f\n", cv::CAP_PROP_FOCUS, cap.get(cv::CAP_PROP_FOCUS));
//	printf("%i,%f\n", cv::CAP_PROP_GUID, cap.get(cv::CAP_PROP_GUID));
//	printf("%i,%f\n", cv::CAP_PROP_ISO_SPEED, cap.get(cv::CAP_PROP_ISO_SPEED));
//	printf("%i,%f\n", cv::CAP_PROP_BACKLIGHT, cap.get(cv::CAP_PROP_BACKLIGHT));
//	printf("%i,%f\n", cv::CAP_PROP_PAN, cap.get(cv::CAP_PROP_PAN));
//	printf("%i,%f\n", cv::CAP_PROP_TILT, cap.get(cv::CAP_PROP_TILT));
//	printf("%i,%f\n", cv::CAP_PROP_ROLL, cap.get(cv::CAP_PROP_ROLL));
//	printf("%i,%f\n", cv::CAP_PROP_IRIS, cap.get(cv::CAP_PROP_IRIS));
//	printf("%i,%f\n", cv::CAP_PROP_BUFFERSIZE,
//			cap.get(cv::CAP_PROP_BUFFERSIZE));
//	printf("%i,%f\n", cv::CAP_PROP_AUTOFOCUS, cap.get(cv::CAP_PROP_AUTOFOCUS));
//	printf("%i,%f\n", cv::CAP_PROP_SAR_NUM, cap.get(cv::CAP_PROP_SAR_NUM)); //!< Sample aspect ratio: num/den (num)
//	printf("%i,%f\n", cv::CAP_PROP_SAR_DEN, cap.get(cv::CAP_PROP_SAR_DEN)); //!< Sample aspect ratio: num/den (den)
//	printf("=========================================\n");
//	char camera_name[80];
//	sprintf(camera_name, "Camera[%i]", 0);
//	cv::namedWindow(camera_name, cv::WINDOW_AUTOSIZE);
//	int fps = 0;
//	auto start = std::chrono::system_clock::now();
//	for (;;) {
//		cap >> src;
//		fps++;
////		cv::imshow(camera_name, src);
//		if (fps == 40) {
//			auto finish = std::chrono::system_clock::now();
//			std::chrono::duration<double> elapsedTime = finish - start;
//			printf("FPS[%i] = %f\n", 0, 40 / elapsedTime.count());
//			start = finish;
//			fps = 0;
//		}
////		cv::waitKey(1);
//	}
//
////	struct sigaction sa;
////	struct itimerval timer;
////		/* Install timer_handler as the signal handler for SIGVTALRM. */
////		memset(&sa, 0, sizeof(sa));
////		sa.sa_handler = &timer_handler;
////		sigaction(SIGVTALRM, &sa, NULL);
////		/* Configure the timer to expire after 250 msec... */
////		timer.it_value.tv_sec = 1;
////		timer.it_value.tv_usec = 0; //250000;
////		/* Start a virtual timer. It counts down whenever this process is
////		 executing. */
////		if(setitimer(ITIMER_VIRTUAL, &timer, NULL)==-1){
////			printf("Failed to create timers!");
////			return 0;
////		}
////
////
////		for (;;) {
////			cap >> src;
////			fps++;
////	//		cv::imshow(camera_name, src);
////			if (done) {
////				printf("FPS[%i] = %f\n", 0, fps);
////				done = 0;
////				fps = 0;
////			}
////			//cv::waitKey(1);
////		}
//
//	// Variable used for opencv
////	cv::VideoCapture cap("nvcamerasrc ! video/x-raw(memory:NVMM), width=(int)1280, height=(int)720, format=(string)I420, framerate=(fraction)120/1 ! \
//	////                    nvvidconv   ! video/x-raw, format=(string)BGRx ! \
//	////                    videoconvert ! video/x-raw, format=(string)BGR ! \
//	////                    appsink");
//	return (0);
//}
