//// Add for camera input
//#include "opencv2/opencv.hpp"
//
//// This is for the definition of cv::mat
//#include "opencv2/core/core.hpp"
//
//// This is for the imread function
//#include "opencv2/imgcodecs.hpp"
//
//// Image processing functions with the cvtColor
//#include "opencv2/imgproc/imgproc.hpp"
//
//// Display the window with results
//#include "opencv2/highgui/highgui.hpp"
//
//// Pthread for the multithreading
//#include <pthread.h>
//
//#include <stdio.h>
//
//#include <chrono>
//
//#define tot_num_camera 3
//
//// This is for passing to pthread as arguments
//struct Cpu_thread_struct {
//	int thread_id;
//	volatile bool done;
//};
//
///* The threads */
//void *camera(void *arguments) {
//	struct Cpu_thread_struct* cpu_thread_struct =
//			(struct Cpu_thread_struct*) arguments;
//	cv::Mat src;
//	cv::VideoCapture cap;
//	switch (cpu_thread_struct->thread_id) {
//	case 0:
//		cap = cv::VideoCapture(0);
//		break;
//	case 1:
//		cap = cv::VideoCapture(
//				"rtsp://admin:admin123@192.168.1.62:554/Streaming/channels/1"); // 30fps
//		break;
//	case 2:
//		cap = cv::VideoCapture(
//				"rtsp://admin:admin123@192.168.1.66:554/Streaming/channels/1"); // 60fps
//		break;
//	default:
//		break;
//	}
//	if (!cap.isOpened()) {
//		return NULL;
//	}
//	char camera_name[80];
//	sprintf(camera_name, "Camera[%i]", cpu_thread_struct->thread_id);
//	cv::namedWindow(camera_name, cv::WINDOW_AUTOSIZE);
//	int fps = 0;
//	auto start = std::chrono::system_clock::now();
//	for (;;) {
//		cap >> src;
//		fps++;
//		cv::imshow(camera_name, src);
//		auto finish = std::chrono::system_clock::now();
//		std::chrono::duration<double> elapsedTime = finish - start;
//		if (elapsedTime.count() > 1) {
//			printf("FPS[%i] = %i\n", cpu_thread_struct->thread_id, fps);
//			start = finish;
//			fps = 0;
//		}
//		cv::waitKey(1);
//	}
//	/* the function must return something - NULL will do */
//	return NULL;
//}
//
//int main(int, char** argv) {
//	pthread_t camera_thread[tot_num_camera];
//	struct Cpu_thread_struct cpu_thread_struct[tot_num_camera];
//	for (int t = 1; t < tot_num_camera; ++t) { // Start from one because the parent is zero
//		cpu_thread_struct[t].thread_id = t;
//		cpu_thread_struct[t].done = false;
//
//		/* create the threads which camera in cpu */
//		if (pthread_create(&camera_thread[t], NULL, camera,
//				(void*) &cpu_thread_struct[t])) {
//			printf("Error creating thread[%i]!\n", t);
//			return -1;
//		}
//	}
//	camera((void*) &cpu_thread_struct[0]);
//	// Variable used for opencv
////	cv::VideoCapture cap("nvcamerasrc ! video/x-raw(memory:NVMM), width=(int)1280, height=(int)720, format=(string)I420, framerate=(fraction)120/1 ! \
//	////                    nvvidconv   ! video/x-raw, format=(string)BGRx ! \
//	////                    videoconvert ! video/x-raw, format=(string)BGR ! \
//	////                    appsink");
//	return (0);
//}
