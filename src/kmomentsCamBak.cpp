////The definition of features exist in the opencv
//#include <opencv2/opencv_modules.hpp>
//
//// Add for camera input
//#include <opencv2/videoio.hpp>
//#include <opencv2/opencv.hpp>
//
//// This is for the definition of cv::mat
//#include <opencv2/core/core.hpp>
//
//// This is for the imread function
//#include <opencv2/imgcodecs.hpp>
//
//// Image processing functions with the cvtColor
//#include <opencv2/imgproc/imgproc.hpp>
//
//// Display the window with results
//#include <opencv2/highgui/highgui.hpp>
//
//#ifdef HAVE_OPENCV_CUDAIMGPROC // We assume if we have this, then the OpenCV is compiled with CUDA
//// CUDA Counterparts:
//#include <opencv2/core/cuda.hpp>
//
//// Image processing functions with the cuda::cvtColor
//#include <opencv2/cudaimgproc.hpp>
//
//// This is for implementing the gaussian blur for cuda
//#include <opencv2/cudafilters.hpp>
//#endif
//// Pthread for the multithreading
//#include <pthread.h>
//
//#include <stdio.h>
//
//// Chrono is for timer
//#include <chrono>
//
//// For getting tid
//#include <sys/syscall.h>   /* For SYS_xxx definitions */
//// For getting pid
//#include <unistd.h>
//// For setting priority
//#include <sys/resource.h>
//
//// get the ubuntu data, etc
//#include <sys/utsname.h>
//
//#include <vector>
////http://www.mathcs.emory.edu/~cheung/Courses/455/Syllabus/5c-pthreads/sync.html
//struct BinarySemaphore {
//	pthread_cond_t cv; /* cond. variable
//	 - used to block threads */
//	pthread_mutex_t mutex; /* mutex variable
//	 - used to prevents concurrent
//	 access to the variable "flag" */
//	int flag; /* Semaphore state:
//	 0 = down, 1 = up */
//};
//
//// This is for passing to pthread as arguments
//struct CannyCPUStruct {
//	int threadId;
//	int workerType; // Two type: CAM_SYNCHRONIZED=0 or CAM_ASYNCHRONIZED=1
//	BinarySemaphore* inBinSema;
//	BinarySemaphore* outBinSema;
//	cv::VideoCapture* cap;
//	cv::Mat* src;
//	cv::Mat* srcGray;
//	cv::Mat* srcBlur;
//	cv::Mat* canny;
//};
//
//#ifdef HAVE_OPENCV_CUDAIMGPROC
//// This is for passing to pthread as arguments
//struct CannyGPUStruct {
//	int threadId;
//	int workerType; // Two type: CAM_SYNCHRONIZED=0 or CAM_ASYNCHRONIZED=1
//	cv::VideoCapture* cap;// This class saves states. Has to be called from the same thread
//	BinarySemaphore* gpuInBinSema;
//	BinarySemaphore* gpuOutBinSema;
//	cv::cuda::GpuMat* gpuSrc;
//	cv::cuda::GpuMat* gpuSrcGray;
//	cv::cuda::GpuMat* gpuSrcBlur;
//	cv::cuda::GpuMat* gpuCanny;
//	cv::Ptr<cv::cuda::CannyEdgeDetector>* ced;
//	cv::Ptr<cv::cuda::Filter>* filter;
//};
//#endif
//
//int thresh = 100;
//#define max_thresh 255
//
///**
// * Note on opencv Mat operation:
// * Mat A = imread(argv[1], CV_LOAD_IMAGE_COLOR);
//
// * Mat B = A.clone();  // B is a deep copy of A. (has its own copy of the pixels)
// * Mat C = A;          // C is a shallow copy of A ( rows, cols copied, but shared pixel-pointer )
// * Mat D; A.copyTo(D); // D is a deep copy of A, like B
// */
//
//// Mutex Definitions:
//#define CAM_SYNCHRONIZED 0 // Means you should use cam[i].grab() in the parent
//#define CAM_ASYNCHRONIZED 1 // Means the thread grabs the image for you (Hence speed).
//
///* The threads */
//void *cannyCPU(void *arguments) {
//	struct CannyCPUStruct* cannyCPUStruct = (struct CannyCPUStruct*) arguments;
//	pid_t tid = syscall(SYS_gettid);
//	printf("tid of CPU[%i] %i\n", cannyCPUStruct->threadId, tid);
//	int ret = setpriority(PRIO_PROCESS, tid, 19); // set as highest niceness (low priority) in ubuntu
//	BinarySemaphore* inBinSema = cannyCPUStruct->inBinSema;
//	BinarySemaphore* outBinSema = cannyCPUStruct->outBinSema;
//	cv::VideoCapture* cap = (cannyCPUStruct->cap);
//	cv::Mat* src = (cannyCPUStruct->src); // shallow copy
////	std::cout << "4.Address or src[0]" << (cannyCPUStruct->src) << std::endl;
////	std::cout << "4z.Address or cv::Mat src (child)" << src << std::endl;
//	cv::Mat* srcGray = (cannyCPUStruct->srcGray); // shallow copy
//	cv::Mat* srcBlur = (cannyCPUStruct->srcBlur); // shallow copy
//	cv::Mat* canny = (cannyCPUStruct->canny); // shallow copy
////	printf("Enter child\n");
////	fflush(stdout);
//	int err;
//	switch (cannyCPUStruct->workerType) {
//	case CAM_SYNCHRONIZED:
//		for (;;) {
//			/* Try to get exclusive access to s->flag */
////			printf("child try pthread_mutex_lock!\n");
////			fflush(stdout);
//			err = pthread_mutex_lock(&inBinSema->mutex);
////			if (err > 1) {
////				printf("child pthread_mutex_lock error %i", err);
////			}
//			/* Success - no other thread can get here unless
//			 the current thread unlock "mutex"              */
////			printf("Child enter mutex with flag %i\n", inBinSema->flag);
//			/* Examine the flag and wait until flag == 1 */
//			while (inBinSema->flag == 0) { // I think we can use if's here
////				printf("Child enter pthread_cond_wait with flag %i\n",
////						inBinSema->flag);
//				err = pthread_cond_wait(&(inBinSema->cv), &(inBinSema->mutex));
////				if (err > 1) {
////					printf("child pthread_cond_wait error %i", err);
////				}
//				/* When the current thread execute this
//				 pthread_cond_wait() statement, the
//				 current thread will be block on s->cv
//				 and (atomically) unlocks s->mutex !!!
//				 Unlocking s->mutex will let other thread
//				 in to test s->flag.... */
////				printf("Child exit conditional wait with flag %i\n",
////						inBinSema->flag);
//			}
////			printf("Child enter main code with flag %i\n", inBinSema->flag);
//			fflush(stdout);
//			/* -----------------------------------------
//			 If the program arrive here, we know that
//			 s->flag == 1 and this thread has now
//			 successfully pass the semaphore !!!
//			 ------------------------------------------- */
//			/**
//			 * Main body
//			 */
////			std::cout << "5.Address or src[0]" << src << std::endl;
////			try {
////				printf("try retrieving!");
////				fflush(stdout);
//			cap->retrieve(src[0], 0);
////				if (!cap->retrieve(src[0], 0))
////				{
////					printf("error retrieving!! Waat!?!?\n");
////					fflush(stdout);
////				}
////				std::cout << "3.Address or src[0]" << &src[0] << std::endl;
////			} catch (int e) {
////				printf("An exception occurred. Exception Nr. %i\n", e);
////				fflush(stdout);
////			}
////			printf("Set flag!");
//			inBinSema->flag = 0;/* This will cause all other threads
//			 that executes a P() call to wait
//			 in the (above) while-loop !!! */
////			printf(
////					"Child exit main code & enter pthread_cond_signal2 with flag %i\n",
////					inBinSema->flag);
////			fflush(stdout);
//			pthread_cond_signal(&(inBinSema->cv)); // To open the other thread
////			printf("Child enter pthread_mutex_unlock with flag %i\n",
////					inBinSema->flag);
////			fflush(stdout);
//			pthread_mutex_unlock(&(inBinSema->mutex));
//
////			printf("Child do gaussian");
////			fflush(stdout);
//			cv::cvtColor(*src, *srcGray, cv::COLOR_BGR2GRAY);
//			cv::GaussianBlur(*srcGray, *srcBlur, cv::Size(3, 3), 0, 0);
////			printf("Child try out pthread_mutex_lock with flag %i\n",
////					outBinSema->flag);
////			fflush(stdout);
//			pthread_mutex_lock(&(outBinSema->mutex));
//			while (outBinSema->flag == 1) {
//				// it's full
////				printf("Child try out pthread_cond_wait with flag %i\n",
////						outBinSema->flag);
////				fflush(stdout);
//				pthread_cond_wait(&outBinSema->cv, &outBinSema->mutex);
////				printf("Child exit out pthread_cond_wait with flag %i\n",
////						outBinSema->flag);
////				fflush(stdout);
//			}
//			outBinSema->flag = 1;
//			/**
//			 * Main body
//			 */
//
//			cv::Canny(*srcGray, *canny, thresh, thresh * 2, 3);
////			printf("Child signal out pthread_cond_signal with flag %i\n",
////					outBinSema->flag);
////			fflush(stdout);
//
//			pthread_cond_signal(&(outBinSema->cv));
////			printf("Child enter out pthread_mutex_unlock with flag %i\n",
////					outBinSema->flag);
////			fflush(stdout);
//			pthread_mutex_unlock(&(outBinSema->mutex));
//		}
//		break;
//	case CAM_ASYNCHRONIZED:
//
//		break;
//	default:
//		break;
//	}
//
//	// Slave thread shall not exit!
//
//	printf("CPU[%i] should not exit!\n", cannyCPUStruct->threadId);
//	/* the function must return something - NULL will do */
//	return NULL;
//}
//
//#ifdef HAVE_OPENCV_CUDAIMGPROC
//void *cannyGPU(void *arguments) {
//	struct CannyGPUStruct* cannyGPUStruct = (struct CannyGPUStruct*) arguments;
//	pid_t tid = syscall(SYS_gettid);
//	printf("tid of GPU[%i] %i\n", cannyGPUStruct->threadId, tid);
//	int ret = setpriority(PRIO_PROCESS, tid, -19); // set as lowest niceness (high priority) in ubuntu
//	cv::cuda::GpuMat* gpuSrc = (cannyGPUStruct->gpuSrc);// shallow copy
//	cv::cuda::GpuMat* gpuSrcGray = *(cannyGPUStruct->gpuSrcGray);// shallow copy
//	cv::cuda::GpuMat* gpuSrcBlur = *(cannyGPUStruct->gpuSrcBlur);// shallow copy
//	cv::cuda::GpuMat* gpuCanny = *(cannyGPUStruct->gpuCanny);// shallow copy
//	cv::Ptr<cv::cuda::CannyEdgeDetector>* ced = (cannyGPUStruct->ced);
//	cv::Ptr<cv::cuda::Filter>* filter = (cannyGPUStruct->filter);
//	cv::cuda::cvtColor(gpuSrc, gpuSrcGray, cv::COLOR_BGR2GRAY);
//	filter->apply(gpuSrcGray, gpuSrcBlur);
//	ced->detect(gpuSrcBlur, gpuCanny);
//	printf("GPU[%i] should not exit!\n", cannyGPUStruct->threadId);
//	/* the function must return something - NULL will do */
//	return NULL;
//}
//#endif
//
//int main(int, char** argv) {
//	// Printing the overall system status:
//	printf("================ OpenCV Status ==================\n");
//	printf("Compiled with: %s\n", CV_VERSION);
//	printf("Running OpenCV:\n");
//	printf("%s\n", cv::getBuildInformation().c_str());
//	printf("OpenCV's number of threads: %i\n", cv::getNumThreads());
//	printf("Total number of threads generated = %i\n", cv::getNumberOfCPUs());
//
//	// Number of processor online (Through system calls):
//	int totNumThread = sysconf(_SC_NPROCESSORS_ONLN); // Including one parent.
//	printf("Total number of processor detected = %i\n", totNumThread);
//
//	if ((totNumThread != cv::getNumberOfCPUs())
//			&& (totNumThread != cv::getNumThreads())) {
//		printf("The numbers above are different! Contact support!");
//	}
//	// Setting the number of thread:
//	cv::setNumThreads(1);
//	// Compile opencv without openmp ()& cv::setNumThreads(1);
//	printf("==================================================\n");
//	// Checking the scheduler of the linux:
//	const int policy = sched_getscheduler(0);
//	printf("The policy is [");
//	switch (policy) {
//	case SCHED_BATCH:
//		printf("SCHED_BATCH");
//		break;
//	case SCHED_FIFO:
//		printf("SCHED_FIFO");
//		break;
//	case SCHED_IDLE:
//		printf("SCHED_IDLE");
//		break;
//	case SCHED_OTHER:
//		printf("SCHED_OTHER");
//		break;
//	case SCHED_RESET_ON_FORK:
//		printf("SCHED_RESET_ON_FORK");
//		break;
//	case SCHED_RR:
//		printf("SCHED_RR");
//		break;
//	default:
//		printf("unknown sched!");
//		break;
//	}
//	printf("]\n");
//	int maxPrio = sched_get_priority_max(policy);
//	int minPrio = sched_get_priority_min(policy);
//	if ((maxPrio == -1) || (minPrio == -1)) {
//		printf("Receiving priority error!, errno = %d\n", errno);
//		return (1);
//	}
//	printf("Highest priority is = %i\n", maxPrio);
//	printf("Lowest priority is = %i\n", minPrio);
//	printf("Set GPU priority to max\n");
//	printf("Set CPU priority to min\n");
//	pthread_attr_t gPU_t_attr;
//	pthread_attr_t cPU_t_attr;
//	int ret = 0;
//	/* initialized with default attributes */
//	ret = pthread_attr_init(&gPU_t_attr);
//	ret = pthread_attr_init(&cPU_t_attr);
//	sched_param gPU_param;
//	sched_param cPU_param;
//	/* safe to get existing scheduling param */
//	ret = pthread_attr_getschedparam(&gPU_t_attr, &gPU_param);
//	ret = pthread_attr_getschedparam(&cPU_t_attr, &cPU_param);
//	/* set the priority; others are unchanged */
//	gPU_param.sched_priority = maxPrio;
//	cPU_param.sched_priority = minPrio;
//	/* setting the new scheduling param */
//	ret = pthread_attr_setschedparam(&gPU_t_attr, &gPU_param);
//	ret = pthread_attr_setschedparam(&gPU_t_attr, &cPU_param);
//	///////////////////////////////////////////////
//	// Niceness value: (when the policy is other)
//	// Check and set this thread priority first by these code:
//	// Get the thread ID
//	pid_t tid = syscall(SYS_gettid);
//	if (tid == -1) {
//		printf("Failed to get the thread ID, errno = %d\n", errno);
//		return (1);
//	}
//	ret = getpriority(PRIO_PROCESS, tid);
//	printf("tid of Parent %i\n", tid);
//	printf("Parent's niceness %i\n", ret);
//	// Show all available camera:
//	printf("===============All camera:================\n");
//	std::vector<std::string*> cameras;
//	int strSize = 200;
//	FILE* pipe = popen("ls /dev/video*", "r");
//	if (!pipe) {
//		printf("popen() failed!");
//		return 1;
//	}
//	char buffer[strSize];	// buffer is 100 chars in length
//	while (fgets(buffer, sizeof(char) * strSize, pipe) != NULL) {
//		std::string* stringBuffer = new std::string;
//		stringBuffer->append(buffer);
//		stringBuffer->pop_back();	// There is a \n included here! so pop it!
//		cameras.push_back(stringBuffer);
//		printf("%s", stringBuffer->c_str());
//	}
//	pclose(pipe);
//	for (int i = 0; i < cameras.size(); ++i) {
//		std::string stringBuffer;
//		stringBuffer.append("udevadm info --query=all --name=");
//		stringBuffer.append(cameras.at(i)->c_str());
//		pipe = popen(stringBuffer.c_str(), "r");
//		if (!pipe) {
//			printf("popen() failed!");
//			return 1;
//		}
//		while (fgets(buffer, sizeof(char) * strSize, pipe) != NULL) {
//			printf("%s", buffer);
//		}
//		pclose(pipe);
//	}
//
//	printf("Make sure to check with the v4l2-ctl --list-devices\n");
//	printf("install by using sudo apt-get install v4l-utils\n");
//
//	///////////////////////////
//
//	cv::Mat src[totNumThread]; // Read only data from the capture structure
//	cv::Mat srcGray[totNumThread];
//	cv::Mat srcBlur[totNumThread];
//	cv::Mat canny[totNumThread];
//	BinarySemaphore inBinSema[totNumThread];
//	BinarySemaphore outBinSema[totNumThread];
//	int err;
//	//Mutex init
//	printf("totNumThread = %i\n", totNumThread);
//	for (int i = 0; i < totNumThread; ++i) {
//		printf("Setting mutex[%i]\n", i);
//		err = pthread_mutex_init(&(inBinSema[i].mutex), NULL);
//		err += pthread_mutex_init(&(outBinSema[i].mutex), NULL);
//		if (err != 0) {
//			printf("pthread_mutex_init [%i] errcode:%i\n", i, err);
//			return 1;
//		}
//		err += pthread_cond_init(&(inBinSema[i].cv), NULL);
//		err += pthread_cond_init(&(outBinSema[i].cv), NULL);
//		if (err != 0) {
//			printf("pthread_cond_init [%i] errcode:%i\n", i, err);
//			return 1;
//		}
//		inBinSema[i].flag = 0;
//		outBinSema[i].flag = 0;
//	}
//
////	std::cout << "1.Address or src[0]" << &src[0] << std::endl;
//
//	char camera_name[20];
//	// Video capture:
//	cv::VideoCapture cap[cameras.size()];
//	for (int i = 0; i < cameras.size(); ++i) { // Open all cameras
//		cap[i].open(0); //*cameras.at(0)
//		if (!cap[i].isOpened()) {
//			printf("No camera is connected!!");
//			return 0;
//		}
//		printf("===============Camera[%i] specs:================\n", i);
//		printf("FPS = %f\n", cap[i].get(cv::CAP_PROP_FPS));
//		printf("Width x Height = %fx%f\n", cap[i].get(cv::CAP_PROP_FRAME_WIDTH),
//				cap[i].get(cv::CAP_PROP_FRAME_HEIGHT));
//		printf("%i,%f\n", cv::CAP_PROP_BRIGHTNESS,
//				cap[i].get(cv::CAP_PROP_BRIGHTNESS)); //!< Brightness of the image (only for those cameras that support).
//		printf("%i,%f\n", cv::CAP_PROP_CONTRAST,
//				cap[i].get(cv::CAP_PROP_CONTRAST)); //!< Contrast of the image (only for cameras).
//		printf("%i,%f\n", cv::CAP_PROP_SATURATION,
//				cap[i].get(cv::CAP_PROP_SATURATION)); //!< Saturation of the image (only for cameras).
//		printf("%i,%f\n", cv::CAP_PROP_HUE, cap[i].get(cv::CAP_PROP_HUE)); //!< Hue of the image (only for cameras).
//		printf("%i,%f\n", cv::CAP_PROP_GAIN, cap[i].get(cv::CAP_PROP_GAIN)); //!< Gain of the image (only for those cameras that support).
//		printf("%i,%f\n", cv::CAP_PROP_EXPOSURE,
//				cap[i].get(cv::CAP_PROP_EXPOSURE)); //!
//		printf("%i,%f\n", cv::CAP_PROP_MONOCHROME,
//				cap[i].get(cv::CAP_PROP_MONOCHROME));
//		printf("%i,%f\n", cv::CAP_PROP_SHARPNESS,
//				cap[i].get(cv::CAP_PROP_SHARPNESS));
//		printf("%i,%f\n", cv::CAP_PROP_AUTO_EXPOSURE,
//				cap[i].get(cv::CAP_PROP_AUTO_EXPOSURE)); //!< DC1394: exposure control done by camera, user can adjust reference level using this feature.
//		printf("%i,%f\n", cv::CAP_PROP_GAMMA, cap[i].get(cv::CAP_PROP_GAMMA));
//		printf("%i,%f\n", cv::CAP_PROP_TEMPERATURE,
//				cap[i].get(cv::CAP_PROP_TEMPERATURE));
//		printf("%i,%f\n", cv::CAP_PROP_TRIGGER,
//				cap[i].get(cv::CAP_PROP_TRIGGER));
//		printf("%i,%f\n", cv::CAP_PROP_TRIGGER_DELAY,
//				cap[i].get(cv::CAP_PROP_TRIGGER_DELAY));
//		printf("%i,%f\n", cv::CAP_PROP_WHITE_BALANCE_RED_V,
//				cap[i].get(cv::CAP_PROP_WHITE_BALANCE_RED_V));
//		printf("%i,%f\n", cv::CAP_PROP_ZOOM, cap[i].get(cv::CAP_PROP_ZOOM));
//		printf("%i,%f\n", cv::CAP_PROP_FOCUS, cap[i].get(cv::CAP_PROP_FOCUS));
//		printf("%i,%f\n", cv::CAP_PROP_GUID, cap[i].get(cv::CAP_PROP_GUID));
//		printf("%i,%f\n", cv::CAP_PROP_ISO_SPEED,
//				cap[i].get(cv::CAP_PROP_ISO_SPEED));
//		printf("%i,%f\n", cv::CAP_PROP_BACKLIGHT,
//				cap[i].get(cv::CAP_PROP_BACKLIGHT));
//		printf("%i,%f\n", cv::CAP_PROP_PAN, cap[i].get(cv::CAP_PROP_PAN));
//		printf("%i,%f\n", cv::CAP_PROP_TILT, cap[i].get(cv::CAP_PROP_TILT));
//		printf("%i,%f\n", cv::CAP_PROP_ROLL, cap[i].get(cv::CAP_PROP_ROLL));
//		printf("%i,%f\n", cv::CAP_PROP_IRIS, cap[i].get(cv::CAP_PROP_IRIS));
//		printf("%i,%f\n", cv::CAP_PROP_BUFFERSIZE,
//				cap[i].get(cv::CAP_PROP_BUFFERSIZE));
//		printf("%i,%f\n", cv::CAP_PROP_AUTOFOCUS,
//				cap[i].get(cv::CAP_PROP_AUTOFOCUS));
//		printf("%i,%f\n", cv::CAP_PROP_SAR_NUM,
//				cap[i].get(cv::CAP_PROP_SAR_NUM)); //!< Sample aspect ratio: num/den (num)
//		printf("%i,%f\n", cv::CAP_PROP_SAR_DEN,
//				cap[i].get(cv::CAP_PROP_SAR_DEN)); //!< Sample aspect ratio: num/den (den)
//		printf("=========================================\n");
//
//		sprintf(camera_name, "Camera[%i]", i);
//		cv::namedWindow(camera_name, cv::WINDOW_AUTOSIZE);
//	}
//
////	src = cv::imread("nvidia-logo.jpg");
////	cv::VideoCapture cap("nvcamerasrc ! video/x-raw(memory:NVMM), width=(int)1280, height=(int)720, format=(string)I420, framerate=(fraction)120/1 ! \
////                    nvvidconv   ! video/x-raw, format=(string)BGRx ! \
////                    videoconvert ! video/x-raw, format=(string)BGR ! \
////                    appsink");
////
//
//#ifdef HAVE_OPENCV_CUDAIMGPROC
//	cv::cuda::GpuMat gpuSrc;
//	cv::cuda::GpuMat gpuSrcGray;
//	cv::cuda::GpuMat gpuSrcBlur;
//	cv::cuda::GpuMat gpuCanny;
//
//	cv::Ptr<cv::cuda::CannyEdgeDetector> ced;
//	cv::Ptr<cv::cuda::Filter> filter;
//
//	float cpuLoad = 0.80f;
//	float gpuLoad = 1 - cpuLoad;
//
//	// Skip 1st call gpu api because 1st call might consume lots of time
//	gpuSrc.upload(src);
//	// CED & Filter
//	ced = cv::cuda::createCannyEdgeDetector(thresh, thresh * 2, 3);
//	filter = cv::cuda::createGaussianFilter(gpuSrcGray.type(),
//			gpuSrcBlur.type(), cv::Size(3, 3), 0, 0);
//	// Warmup the GPU
//	cv::cuda::cvtColor(gpuSrc, gpuSrcGray, cv::COLOR_BGR2GRAY);
//	filter->apply(gpuSrcGray, gpuSrcBlur);
//	ced->detect(gpuSrcBlur, gpuCanny);
//#endif
//
//	/**
//	 * CPU Single Parts
//	 */
//	printf("CPU Single parts:\n");
//	pthread_t cannyCPUSoloThread;
//	struct CannyCPUStruct cannyCPUSoloStruct;
//	cannyCPUSoloStruct.threadId = 0;
//	cannyCPUSoloStruct.workerType = CAM_SYNCHRONIZED;
//	cannyCPUSoloStruct.inBinSema = &inBinSema[0];
//	cannyCPUSoloStruct.outBinSema = &outBinSema[0];
//	cannyCPUSoloStruct.cap = &cap[0];
//	cannyCPUSoloStruct.src = &src[0];
////	std::cout << "2.Address or src[0]" << &src[0] << std::endl;
//	cannyCPUSoloStruct.srcGray = &srcGray[0];
//	cannyCPUSoloStruct.srcBlur = &srcBlur[0];
//	cannyCPUSoloStruct.canny = &canny[0];
//
//	/* create the threads which executes canny edge detection in cpu */
//	if (pthread_create(&cannyCPUSoloThread, &cPU_t_attr, cannyCPU,
//			(void*) &cannyCPUSoloStruct)) {
//		printf("Error creating thread[%i]!\n", cannyCPUSoloStruct.threadId);
//		return 1;
//	}
//	int fps = 0;
//	auto start = std::chrono::system_clock::now();
//	for (;;) {
//		for (int i = 0; i < cameras.size(); i++) {
//			/* Try to get exclusive access to s->flag */
////			printf("Parent try pthread_mutex_lock! %i\n", inBinSema[0].flag);
//			err = pthread_mutex_lock(&inBinSema[0].mutex);
////			if (err > 1) {
////				printf("Parent pthread_mutex_lock error %i", err);
////			}
////			printf("Entering parent pthread_mutex_lock\n");
//			while (inBinSema[0].flag == 1) { // I suspect we can use if's here
//				//The consumer is not eat this yet! so stall first
////				printf("Entering parent pthread_cond_wait\n");
//				err = pthread_cond_wait(&inBinSema[0].cv, &inBinSema[0].mutex);
////				printf("Exiting parent pthread_cond_wait\n");
////				if (err > 1) {
////					printf("Parent pthread_cond_wait error %i", err);
////				}
//			}
////			printf("Enter the parent main code with flag %i\n",
////					inBinSema[0].flag);
//
//			/**
//			 * Main body
//			 */
//			fps++;
//
//			if (fps == 40) {
//				auto finish = std::chrono::system_clock::now();
//				std::chrono::duration<double> elapsedTime = finish - start;
//				printf("FPS[%i] = %f\n", 0, 40 / elapsedTime.count());
//				start = finish;
//				fps = 0;
//			}
//
////			printf("exiting FPS block\n");
//
//			if (!cap[i].grab()) {
//				printf("Grabbing [i] failed!\n");
//				return 1;
//			}
//			/* Update semaphore state to Up */
//			inBinSema[0].flag = 1;
////				printf("Set the parent flag %i\n", inBinSema[0].flag);
////				printf("Parent enter pthread_cond_signal!\n");
//			err = pthread_cond_signal(&inBinSema[0].cv);
////				printf("Parent release pthread_cond_signal!\n");
////				if (err > 1) {
////					printf("Parent pthread_cond_signal error %i", err);
////				}
//			/* This call may restart some thread that
//			 was blocked on s->cv (in the P() call)
//			 - if there was not thread blocked on
//			 - cv, this operation does absolutely
//			 - nothing...                          */
//			/* Release exclusive access to s->flag */
////				printf("Parent enter pthread_mutex_unlock!\n");
//			err = pthread_mutex_unlock(&inBinSema[0].mutex);
////				printf("Parent exit pthread_mutex_unlock!\n");
////				if (err > 1) {
////					printf("Parent pthread_mutex_unlock error %i", err);
////				}
////				printf("Parent try out pthread_mutex_lock!\n");
//			pthread_mutex_lock(&outBinSema[0].mutex);
////				printf("Entering parent out pthread_mutex_lock\n");
//			while (outBinSema[0].flag == 0) {
//				// nothing to grab
////					printf("Parent try out pthread_cond_wait!\n");
//				pthread_cond_wait(&(outBinSema[0].cv), &(outBinSema[0].mutex));
////					printf("Parent exit out pthread_cond_wait!\n");
//			}
//
//			/**
//			 * Main body
//			 */
////				std::cout << "6.Address or src[0]" << &src[0] << std::endl;
//			sprintf(camera_name, "Camera[%i]", 0);
//			cv::imshow(camera_name, canny[0]);
//			cv::waitKey(1);
//			outBinSema[0].flag = 0;
//			pthread_cond_signal(&outBinSema[0].cv);
//			pthread_mutex_unlock(&outBinSema[0].mutex);
//
//		}
////		printf("Reloop\n");
//	}
//#ifdef HAVE_OPENCV_CUDAIMGPROC
//	/**
//	 * GPU Single Parts
//	 */
//	pthread_t cannyGPUSoloThread;
//	struct CannyGPUStruct cannyGPUSoloStruct;
//	cannyGPUSoloStruct.gpuSrc = &gpuSrc;
//	cannyGPUSoloStruct.gpuSrcGray = &gpuSrcGray;
//	cannyGPUSoloStruct.gpuSrcBlur = &gpuSrcBlur;
//	cannyGPUSoloStruct.gpuCanny = &gpuCanny;
//	cannyGPUSoloStruct.filter = &filter;
//	cannyGPUSoloStruct.ced = &ced;
//	cannyGPUSoloStruct.threadId = 0;
//
//	auto startGPUSolo = std::chrono::system_clock::now();
//
//	/* create the threads which executes canny edge detection in Gpu */
//	if (pthread_create(&cannyGPUSoloThread, &gPU_t_attr, cannyGPU,
//					(void*) &cannyGPUSoloStruct)) {
//		printf("Error creating thread[%i]!\n", cannyGPUSoloStruct.threadId);
//		return 1;
//	}
//	/* wait for the threads to finish */
//	if (pthread_join(cannyGPUSoloThread, NULL)) {
//		printf("Error joining thread[%i]!\n", cannyCPUSoloStruct.threadId);
//		return 2;
//	}
//	auto finishGPUSolo = std::chrono::system_clock::now();
//	std::chrono::duration<double> elapsedTimeGPUSolo = finishGPUSolo
//	- startGPUSolo;
//	printf("GPU Solo Elapsed Time: %f msecs\n",
//			elapsedTimeGPUSolo.count() * 1000);
//	printf("Time improvement= %f %\n",
//			(elapsedTimeCPUSolo.count() - elapsedTimeGPUSolo.count())
//			/ elapsedTimeCPUSolo.count() * 100);
//#endif
////	/**
////	 * CPU Multi Parts
////	 */
////	pthread_t cannyCPUMultiThread[totNumThread];
////	struct CannyCPUStruct cannyCPUMultiStruct[totNumThread];
////	for (int t = 0; t < totNumThread; ++t) {
////		cannyCPUMultiStruct[t].numOfIteration = totNumIteration / totNumThread; // Split evenly on all core
////		cannyCPUMultiStruct[t].src = &src;
////		cannyCPUMultiStruct[t].srcGray = &srcGray[t];
////		cannyCPUMultiStruct[t].srcBlur = &srcBlur[t];
////		cannyCPUMultiStruct[t].canny = &canny[t];
////		cannyCPUMultiStruct[t].threadId = t;
////	}
////	auto startCPUMulti = std::chrono::system_clock::now();
////
////	/* create the threads which executes canny edge detection in gpu */
////	for (int t = 0; t < totNumThread; ++t) {
////		if (pthread_create(&cannyCPUMultiThread[t], &cPU_t_attr, cannyCPU,
////				(void*) &cannyCPUMultiStruct[t])) {
////			printf("Error creating thread[%i]!\n",
////					cannyCPUMultiStruct[t].threadId);
////			return 1;
////		}
////	}
////	/* wait for the threads to finish */
////	for (int t = 0; t < totNumThread; ++t) {
////		if (pthread_join(cannyCPUMultiThread[t], NULL)) {
////			printf("Error joining thread[%i]!\n",
////					cannyCPUMultiStruct[t].threadId);
////			return 2;
////		}
////	}
////	auto finishCPUMulti = std::chrono::system_clock::now();
////	std::chrono::duration<double> elapsedTimeCPUMulti = finishCPUMulti
////			- startCPUMulti;
////	printf("CPU Multi Elapsed Time: %f msecs\n",
////			elapsedTimeCPUMulti.count() * 1000);
////	printf("Time improvement= %f %\n",
////			(elapsedTimeCPUSolo.count() - elapsedTimeCPUMulti.count())
////					/ elapsedTimeCPUSolo.count() * 100);
//
//#ifdef HAVE_OPENCV_CUDAIMGPROC
//	/**
//	 * Hybrids 2 cpu thread + 1 gpu (Parent allocate the gpu)
//	 */
//	pthread_t cannyCPUSoloGPUThread;
//	struct CannyCPUStruct cannyCPUSoloGPUStruct;
//	cannyCPUSoloGPUStruct.src = &src;
//	cannyCPUSoloGPUStruct.srcGray = &srcGray[0];
//	cannyCPUSoloGPUStruct.srcBlur = &srcBlur[0];
//	cannyCPUSoloGPUStruct.canny = &canny[0];
//	cannyCPUSoloGPUStruct.threadId = 0;
//
//	pthread_t cannyCPUSoloGPUThread2;
//	struct CannyGPUStruct cannyCPUSoloGPUStruct2;
//	cannyCPUSoloGPUStruct2.gpuSrc = &gpuSrc;
//	cannyCPUSoloGPUStruct2.gpuSrcGray = &gpuSrcGray;
//	cannyCPUSoloGPUStruct2.gpuSrcBlur = &gpuSrcBlur;
//	cannyCPUSoloGPUStruct2.gpuCanny = &gpuCanny;
//	cannyCPUSoloGPUStruct2.ced = &ced;
//	cannyCPUSoloGPUStruct2.filter = &filter;
//	cannyCPUSoloGPUStruct2.threadId = 1;
//
//	auto startCPUSoloGPUHybrid = std::chrono::system_clock::now();
//
//	/* create the threads which executes canny edge detection in cpu */
//
//	if (pthread_create(&cannyCPUSoloGPUThread, &cPU_t_attr, cannyCPU,
//					(void*) &cannyCPUSoloGPUStruct)) {
//		printf("Error creating thread[%i]!\n", cannyCPUSoloGPUStruct.threadId);
//		return 1;
//	}
//
//	if (pthread_create(&cannyCPUSoloGPUThread2, &gPU_t_attr, cannyGPU,
//					(void*) &cannyCPUSoloGPUStruct2)) {
//		printf("Error creating thread[%i]!\n", cannyCPUSoloGPUStruct2.threadId);
//		return 1;
//	}
//	/* wait for the threads to finish */
//	if (pthread_join(cannyCPUSoloGPUThread2, NULL)) {
//		printf("Error joining thread[%i]!\n", 1);
//		return 2;
//	}
//	auto finishGPU = std::chrono::system_clock::now();
//	std::chrono::duration<double> elapsedTimeGPU = finishGPU
//	- startCPUSoloGPUHybrid;
//	printf("GPU Elapsed Time: %f msecs\n", elapsedTimeGPU* 1000);
//	/* wait for the threads to finish */
//	if (pthread_join(cannyCPUSoloGPUThread, NULL)) {
//		printf("Error joining thread[%i]!\n", 0);
//		return 2;
//	}
//
//	auto finishCPUSoloGPUMulti = std::chrono::system_clock::now();
//	std::chrono::duration<double> elapsedTimeCPUSoloGPUMulti =
//	finishCPUSoloGPUMulti - startCPUSoloGPUHybrid;
//	printf("CPUSoloGPU Hybrid Elapsed Time: %f msecs\n",
//			elapsedTimeCPUSoloGPUMulti.count() * 1000);
//	printf("Time improvement= %f %\n",
//			(elapsedTimeCPUSolo.count() - elapsedTimeCPUSoloGPUMulti.count())
//			/ elapsedTimeCPUSolo.count() * 100);
//
//	/**
//	 * Hybrids 3 cpu + 1 gpu (Parent allocate the gpu)
//	 * Ratio for caanny edge detection in tx2 is 0.46 inst GPU and 0.56 int CPU
//	 */
//	pthread_t cannyCPUGPUThread[totNumThread - 1];
//	struct CannyCPUStruct cannyCPUGPUStruct[totNumThread - 1];
//	for (int t = 0; t < totNumThread - 1; ++t) {
//		cannyCPUGPUStruct[t].numOfIteration = (int) ((float) totNumIteration
//				* 0.80f) / (totNumThread - 1);
//		cannyCPUGPUStruct[t].src = &src;
//		cannyCPUGPUStruct[t].srcGray = &srcGray[t];
//		cannyCPUGPUStruct[t].srcBlur = &srcBlur[t];
//		cannyCPUGPUStruct[t].canny = &canny[t];
//		cannyCPUGPUStruct[t].threadId = t;
//	}
//
//	pthread_t cannyCPUGPUThread2;
//	struct CannyGPUStruct cannyCPUGPUStruct2;
//	cannyCPUGPUStruct2.numOfIteration =
//	(int) ((float) totNumIteration * 0.20f/*gpuLoad*/);
//	cannyCPUGPUStruct2.gpuSrc = &gpuSrc;
//	cannyCPUGPUStruct2.gpuSrcGray = &gpuSrcGray;
//	cannyCPUGPUStruct2.gpuSrcBlur = &gpuSrcBlur;
//	cannyCPUGPUStruct2.gpuCanny = &gpuCanny;
//	cannyCPUGPUStruct2.ced = &ced;
//	cannyCPUGPUStruct2.filter = &filter;
//	cannyCPUGPUStruct2.threadId = totNumThread - 1;
//
//	auto startCPUGPUHybrid = std::chrono::system_clock::now();
//
//	/* create the threads which executes canny edge detection in cpu */
//	for (int t = 0; t < totNumThread - 1; ++t) {
//		if (pthread_create(&cannyCPUGPUThread[t], &cPU_t_attr, cannyCPU,
//						(void*) &cannyCPUGPUStruct[t])) {
//			printf("Error creating thread[%i]!\n",
//					cannyCPUGPUStruct[t].threadId);
//			return 1;
//		}
//	}
//	if (pthread_create(&cannyCPUGPUThread2, &gPU_t_attr, cannyGPU,
//					(void*) &cannyCPUGPUStruct2)) {
//		printf("Error creating thread[%i]!\n", cannyCPUGPUStruct2.threadId);
//		return 1;
//	}
//	if (pthread_join(cannyCPUGPUThread2, NULL)) {
//		printf("Error joining thread[%i]!\n", cannyCPUGPUStruct2.threadId);
//		return 2;
//	}
//	auto finishGPU2 = std::chrono::system_clock::now();
//	std::chrono::duration<double> elapsedTimeGPU2 = finishGPU2
//	- startCPUGPUHybrid;
//	printf("GPU Elapsed Time: %f msecs\n", elapsedTimeGPU2* 1000);
//	/* wait for the threads to finish */
//	for (int t = 0; t < totNumThread - 1; ++t) {
//		if (pthread_join(cannyCPUGPUThread[t], NULL)) {
//			printf("Error joining thread[%i]!\n",
//					cannyCPUGPUStruct[t].threadId);
//			return 2;
//		}
//	}
//
//	auto finishCPUGPUHybrid = std::chrono::system_clock::now();
//	std::chrono::duration<double> elapsedTimeCPUGPUHybrid = finishCPUGPUHybrid
//	- startCPUGPUHybrid;
//	printf("CPUGPU Hybrid Elapsed Time: %f msecs\n",
//			elapsedTimeCPUGPUHybrid.count() * 1000);
//	printf("Time improvement= %f %\n",
//			(elapsedTimeCPUSolo.count() - elapsedTimeCPUGPUHybrid.count())
//			/ elapsedTimeCPUSolo.count() * 100);
//
//	/**
//	 * Hybrids 4 cpu + 1 cpu + 1 gpu (One more thread.)
//	 * This is to test whether we were able to context switch fast enough
//	 * Ratio for canny edge detection in tx2 is 0.46 inst GPU and 0.56 int CPU
//	 */
//
//	pthread_t cannyCPUMultiGPUThread[totNumThread];
//	struct CannyCPUStruct cannyCPUMultiGPUStruct[totNumThread];
//	for (int t = 0; t < totNumThread; ++t) {
//		cannyCPUMultiGPUStruct[t].numOfIteration =
//		(int) ((float) totNumIteration * cpuLoad) / (totNumThread);
//		cannyCPUMultiGPUStruct[t].src = &src;
//		cannyCPUMultiGPUStruct[t].srcGray = &srcGray[t];
//		cannyCPUMultiGPUStruct[t].srcBlur = &srcBlur[t];
//		cannyCPUMultiGPUStruct[t].canny = &canny[t];
//		cannyCPUMultiGPUStruct[t].threadId = t;
//	}
//	pthread_t cannyCPUMultiGPUThread2;
//	struct CannyGPUStruct cannyCPUMultiGPUStruct2;
//	cannyCPUMultiGPUStruct2.numOfIteration = (int) ((float) totNumIteration
//			* gpuLoad);
//	cannyCPUMultiGPUStruct2.gpuSrc = &gpuSrc;
//	cannyCPUMultiGPUStruct2.gpuSrcGray = &gpuSrcGray;
//	cannyCPUMultiGPUStruct2.gpuSrcBlur = &gpuSrcBlur;
//	cannyCPUMultiGPUStruct2.gpuCanny = &gpuCanny;
//	cannyCPUMultiGPUStruct2.threadId = totNumThread;
//	cannyCPUMultiGPUStruct2.ced = &ced;
//	cannyCPUMultiGPUStruct2.filter = &filter;
//
//	auto startCPUMultiGPUHybrid = std::chrono::system_clock::now();
//
//	/* create the threads which executes canny edge detection in cpu */
//	for (int t = 0; t < totNumThread; ++t) {
//		if (pthread_create(&cannyCPUMultiGPUThread[t], &cPU_t_attr, cannyCPU,
//						(void*) &cannyCPUMultiGPUStruct[t])) {
//			printf("Error creating thread[%i]!\n",
//					cannyCPUMultiGPUStruct[t].threadId);
//			return 1;
//		}
//	}
//	/* create the threads which executes canny edge detection in gpu */
//	if (pthread_create(&cannyCPUMultiGPUThread2, &gPU_t_attr, cannyGPU,
//					(void*) &cannyCPUMultiGPUStruct2)) {
//		printf("Error creating thread[i%]!\n",
//				cannyCPUMultiGPUStruct2.threadId);
//		return 1;
//	}
//
//	if (pthread_join(cannyCPUMultiGPUThread2, NULL)) {
//		printf("Error joining thread[%i]!\n", cannyCPUMultiGPUStruct2.threadId);
//		return 2;
//	}
//	auto finishGPU3 = std::chrono::system_clock::now();
//	std::chrono::duration<double> elapsedTimeGPU3 = finishGPU3
//	- startCPUMultiGPUHybrid;
//	printf("GPU Elapsed Time: %f msecs\n", elapsedTimeGPU3* 1000);
//	/* wait for the threads to finish */
//	for (int t = 0; t < totNumThread; ++t) {
//		if (pthread_join(cannyCPUMultiGPUThread[t], NULL)) {
//			printf("Error joining thread[%i]!\n",
//					cannyCPUMultiGPUStruct[t].threadId);
//			return 2;
//		}
//	}
//
//	auto finishCPUMultiGPUHybrid = std::chrono::system_clock::now();
//	std::chrono::duration<double> elapsedTimeCPUMultiGPUHybrid =
//	finishCPUMultiGPUHybrid - startCPUMultiGPUHybrid;
//	printf("CPUGPU Multi Elapsed Time: %f msecs\n",
//			elapsedTimeCPUMultiGPUHybrid.count() * 1000);
//	printf("Time improvement= %f %\n",
//			(elapsedTimeCPUSolo.count() - elapsedTimeCPUMultiGPUHybrid.count())
//			/ elapsedTimeCPUSolo.count() * 100);
//#endif
//	printf("Done!\n");
////	gpuCanny.download(src[0]);
//
////	cv::namedWindow("CPU Canny Edge", cv::WINDOW_AUTOSIZE);
////	cv::imshow("CPU Canny Edge", canny[0]);
////	cv::namedWindow("GPU Canny Edge", cv::WINDOW_AUTOSIZE);
////	cv::imshow("GPU Canny Edge", src[0]);
////	cv::waitKey(0);
//	return (0);
//}
