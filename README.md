Note for opencv4.0.0, do not build with OpenMP, there is a bug.
Tested with 3.4.3
This project uses the OpenCV.

To run this:
1. Follow this tutorial https://www.learnopencv.com/install-opencv3-on-ubuntu/
Note: No need to install virtual environment
2. Use this repo for your Test.
Done.

Common cmake command for opencv builds:

cmake -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=/usr/local -D INSTALL_C_EXAMPLES=OFF -D WITH_CUDA=OFF -D WITH_OPENCL=ON -D WITH_GTK=ON -D WITH_ITT=ON -D WITH_IPP=ON -D WITH_OPENMP=ON -D WITH_OPENGL=ON ..

cmake -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=/usr/local -D INSTALL_C_EXAMPLES=OFF -D WITH_CUDA=OFF -D WITH_OPENCL=ON -D WITH_GTK=ON -D WITH_ITT=OFF -D WITH_IPP=OFF -D WITH_OPENMP=OFF -D WITH_OPENGL=ON ..

etc


Note for video capture, you need to use v4l2 packagein linux.

To use camera, ussualy the path is in /dev/video*
(Opencv supported codecs are at cap_v4l.cpp in videoio folder)
Ussualy we use BGR24 format:
sudo ffmpeg -f x11grab -r 30 -s 1366x768 -i :0.0+0,0 -vcodec rawvideo -pix_fmt bgr24 -threads 0 -f v4l2 /dev/video1

To loopback the camera to be used for videocap, install 
sudo apt-get install v4l2loopback-utils
In the tx2, when you install, there is error:
scripts/basic/fixdep: 1: scripts/basic/fixdep: Syntax error: "(" unexpected

When it happens, go to the:
cd /usr/src/linux-headers-`uname -r`
sudo make modules_prepare

then:
cd /var/lib/dkms/v4l2loopback/0.9.1/build
sudo make
sudo make install

sudo modprobe v4l2loopback
then forward it using above format

To stream, see https://trac.ffmpeg.org/wiki/StreamingGuide
use v4l2-ctl -d /dev/video0 --list-formats to check the source supported format

ffmpeg -re -i 'ffmpeg_stream.mp4'  -r 15 -vcodec rawvideo -f v4l2 /dev/video0

ffmpeg -f pulse -i default -f video4linux2 -thread_queue_size 64 -framerate 25 -video_size 640x480 -i /dev/video0 -pix_fmt yuv420p -bsf:v h264_mp4toannexb -profile:v baseline -level:v 3.2 -c:v libx264 -x264-params keyint=120:scenecut=0 -c:a aac -b:a 128k -ar 44100 -f rtsp -muxdelay 0.1 rtsp://localhost:8554/live/paul

ffmpeg -re -i '/media/therangersolid/49edb8cc-1751-4bb5-8e1d-ba4936a1f37e/samba_share/1.mp4'  -r 15 -vcodec mpeg4 -f mpegts  udp://192.168.1.88:23000?pkt_size=1316

You can use
